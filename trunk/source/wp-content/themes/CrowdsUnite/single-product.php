<?php
global $helper, $wp_query, $post, $allowedtags, $allowedposttags, $current_user, $more;
include PATH_PROCESSING . DS . 'single_product_processing.php';
count_view_product($post->ID);
get_header();

$product_website = '';
$product_images = '';
$product_price = '';

if (get_post_meta($post->ID, 'tgt_product_url', true) != '')
    $product_website = get_post_meta($post->ID, 'tgt_product_url', true);
// Get product website
/*if(get_post_meta($post->ID,'tgt_product_images',true) != '')
    $product_images = get_post_meta($post->ID,'tgt_product_images',true);// Get product images*/
if (get_post_meta($post->ID, 'tgt_product_price', true) != '')
    $product_price = get_post_meta($post->ID, 'tgt_product_price', true);

// Get product price
$post_date = explode(' ', $post->post_date);
$view_post = get_post_meta($post->ID, PRODUCT_VIEW_COUNT, true);
if (empty($view_post))
    $view_post = 0;
$tab_content = get_post_meta($post->ID, 'tgt_tab', true);
$open_comment = comments_open();

// get the picture for the snapshot and logo
$attachments = get_children(array(
    'post_parent' => $post->ID,
    'post_type' => 'attachment',
    'post_mime_type' => 'image',
    'orderby' => 'menu_order',
    'order' => 'ASC'
));


if ( !empty( $attachments ) && is_array( $attachments ) ) {
    $attachments = array_values( $attachments );
} else {
    echo $helper->image('no_image.jpg', 'No Image');
}

if (get_option(PRODUCT_SHOW_RATING, 1) == 1){
    $count = (int)get_post_meta($post->ID, PRODUCT_EDITOR_RATING_COUNT, true);
} else {
    $count = (int)get_post_meta($post->ID, PRODUCT_RATING_COUNT, true);
}

?>
<div id="container">
    <div class="container_res">
        <div class="container_main" itemscope itemtype="http://schema.org/WebPage">
            <!-- Notification Sidebar -->
            <?php
            if (is_active_sidebar('homepage-widget-area')) {
                dynamic_sidebar('homepage-widget-area');
            }
            ?>
            <meta itemprop="name" content="<?php echo $post->post_title; ?>" />

            <!-- End Notification Sidebar -->
            <div class="content_product">
                <div class="col_box" style="width:998px;">
                    <div class="text3" style="margin-left:0px;">
                        <div class="title3" style="width:998px;">
                            <div class="title_left">
                                <div class="title_left">
                                    <span class="item">
                                        <span class="fn">
                                            <h2>
                                                <a itemprop="url" href="<?php echo get_post_meta($post->ID, 'tgt_product_url', true); ?>"><?php echo ( empty( $attachments[1] ) )?$post->post_title:'<img src="'.wp_get_attachment_image_src( $attachments[1]->ID, "medium" )[0].'" alt="'.$post->post_title.'" />';?></a>
                                                <?php if (current_user_can('edit_others_posts')):?>
                                                <a href="<?php echo get_edit_post_link( $post->ID )?>"><img class="edit_link" src="<?php echo TEMPLATE_URL . '/images/pencil.png'; ?>" alt="edit" title="<?php _e('Edit', 're') ?>" /></a>
                                                <?php endif?>
                                            </h2>
                                        </span>
                                    </span>
                                </div>
                            </div>

                            <div class="title_right" style="float:right;">
                                <?php if ( is_user_logged_in() || get_option(SETTING_SUBMIT_WITHOUT_LOGIN) ):?>
                                <a class="btn" href="#reviewModal" data-toggle="modal" style="float:left;margin:15px;"> Write Review </a>
                                <?php else: ?>
                                <a class="btn" href=" <?php echo tgt_get_permalink('register') ?> " style="float:left;margin:15px;"> Write Review </a>
                                <?php endif;?>
                                <div class="vote_star" style="float:right;">
                                    <div class="star">
                                        <p class="date">
                                            <span>
                                                <a href="#tc_review"><?php
                                                        if ($count == 0)
                                                            echo __('This product hasn\'t been reviewed yet', 're');
                                                        else if ($count == 1)
                                                            echo __('One Review', 're');
                                                        else
                                                            echo sprintf(__(' %s Reviews', 're'), $count);

                                                        tgt_display_rating($product['rating']['user']['rating'], 'product[rating][user]');
                                                ?></a>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <script type="text/javascript">
                                jQuery('.star-disabled').rating();
                            </script>

                            <!-- Display rating of product -->
                            <div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                                <meta itemprop="reviewCount" content="<?php echo $count ?>" />

                                <?php $rating = get_post_meta( $post->ID, get_showing_rating(), true ); ?>
                                <meta itemprop="ratingValue" content="<?php echo $rating ?>" />
                                <meta itemprop="bestRating" content="10"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content">
                    <div class="row-fluid">
                        <div class="span5 general">
                            <div class="slide_show" style="margin-bottom: 20px;">
                                <div class="big-photos">
                                    <?php echo ( empty( $attachments[0] ) )?$helper->image('no_image.jpg', 'No Image'):'<img src="'.wp_get_attachment_image_src($attachments[0]->ID, "large")[0].'" alt="'.get_the_title().'"/>';?>
                                </div>
                                <!-- class-slideshow -->
                            </div>
                            <div id="product_intro">
                                <?php
                                setup_postdata($post);
                                $more = 0;
                                the_content("<p><strong>" . __('More info', 're') . "</strong></p>");
                                ?>
                            </div>
                            <div id="product_desc" style="display: none">
                                <?php
                                $more = 1;
                                the_content();
                                ?>
                                <p>
                                    <a class="hide-desc" href="#product"><strong><?php _e('Hide description', 're') ?></strong></a>
                                </p>
                            </div>
                        </div>

                        <div class="span4">
                            <div>
                                <?php include PATH_PAGES . DS . 'product_spec.php'; ?>
                                <div class="content_text" style="margin-top:25px;"></div>
                            </div>
                        </div>
                        <div class="span3 general">
                            <a href="http://www.jdoqocy.com/click-7193091-11295012" style="float:right;"><img src="http://www.awltovhc.com/image-7193091-11295012" class="alignnone" /></a>
                            <a href="http://www.crowdhut.net/" style="float:right;margin-top: 15px;"><img src="http://crowdsunite.com/wp-content/uploads/2013/08/CrowdHut_banner_ad_125px.jpg" class="alignnone" /></a>
                        </div>

                    </div>
                </div>

                <div class="clear"></div>

                <div class="content">
                    <div class="tab">
                        <div class="tab_link">
                            <!--ul class="nav nav-tabs"-->
                            <ul>
                                <!--li id="tab_spec" class="tab-item"><a href="#tc_spec"><?php _e('Specifications','re'); ?></a></li-->
                                <li id="tab_review" class="tab-item"><a href="#tc_review"> <?php _e('Reviews', 're'); ?> </a></li>

                                <!--li id="tab_write" class="tab-item"><a href="#tc_write"><?php _e('Write review','re'); ?> </a></li-->

                                <?php if (get_option(SETTING_ENABLE_ARTICLE)): ?>
                                <li id="tab_articles" class="tab-item"><a href="#tc_article"><?php _e('Articles', 're'); ?></a></li>
                                <?php endif; ?>
                                <?php if ($open_comment == true): ?>
                                <li id="tab_comment" class="tab-item"><a href="#tc_comment"><?php _e('Discussions', 're'); ?></a></li>
                                <?php endif; ?>

                                <?php
                                    $result = get_tab_data_tgt();
                                    if (!empty($result)) {
                                        foreach ($result as $k_g => $v_g) {
                                            $content = '';
                                            if (!empty($tab_content['tab_' . $v_g['ID']])) {
                                ?>
                                <li id="additional_tab_<?php echo $v_g['ID']; ?>" class="tab-item additional-tab">
                                    <a href="#tc_addition_<?php echo $v_g['ID']; ?>">
                                        <?php echo $v_g['name']; ?>
                                    </a>
                                </li>
                                <?php
                                            }
                                        }
                                    }
                                ?>
                            </ul>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div id="tc_review" class="review_tab_content" style="border: medium none;">
                        <?php comments_template('/pages/product_reviews.php', true, false);?>
                    </div>

                    <?php if ($open_comment == true): ?>
                    <div class="review_tab_content" id="tc_comment" style="display:none;">
                        <?php comments_template('/pages/product_writecomment.php', true); ?>
                    </div>
                    <?php endif; ?>

                    <?php if (get_option(SETTING_ENABLE_ARTICLE)): ?>
                    <div class="review_tab_content" id="tc_article" style="display:none;">
                        <?php include PATH_PAGES . DS . 'product_articles.php' ?>
                    </div>
                    <?php endif; ?>

                    <?php include PATH_PAGES . DS . 'product_writereview.php' ?>

                    <?php
                        if (!empty($result)) {
                            foreach ($result as $k_g => $v_g) {
                                $content = get_post_meta($post->ID, 'tgt_tab', true);
                                $content = $tab_content;

                                if (!empty($content)) {
                                    $content = $content['tab_' . $v_g['ID']];
                                    if (!empty($content)) {
                    ?>
                    <div class="review_tab_content" id="tc_addition_<?php echo $v_g['ID']; ?>"
                         style="border: medium none;display:none; padding: 10px 0 0 5px;">
                        <div class="content_review">
                            <div class="revieww post_content" style="padding-bottom: 20px;">
                                <?php echo '<p>' . preg_replace('/<br\s*\/>/', '</p><p>', $content) . '</p>'; ?>
                            </div>
                        </div>
                    </div>
                    <?php
                                    }
                                }
                            }
                        }
                    ?>

                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        // open comment if has #comment-* in url
        pattern = /^#comment-/;
        if (pattern.test(window.location.hash))
            jQuery('#tab_comment > a').trigger('click');

        jQuery('.more-link').click(function () {
            //var target = jQuery(this).attr('href');
            jQuery('#product_intro').hide();
            jQuery('#product_desc').show();
            return false;
        });
        jQuery('.hide-desc').click(function () {
            //var target = jQuery(this).attr('href');
            jQuery('#product_desc').hide();
            jQuery('#product_intro').show();
            return false;
        });

        //
        jQuery('.star-disabled').rating();

        if (jQuery('.no-review').length != 0) {
            jQuery('.review_tab_content').hide();
            jQuery('.tab-item').removeClass('select');
            jQuery('.additional-tab').removeClass('select');
            jQuery('#tab_spec').addClass('select');
            jQuery('#tc_article').show();
        }

        jQuery('.product-small-thumbnail').click(function () {
            var current = jQuery(this),
                id = current.attr('href'),
                target = jQuery(id),
                big_photos = jQuery('.product-big-photo'),
                selected = jQuery('.big-photos .selected');

            selected.fadeOut(500, function () {
                selected.removeClass('selected');
                target.fadeIn(500, function () {
                    jQuery(this).addClass('selected')
                });
            });
            return false;
        });

        jQuery('#thumb_slide_list').each(function () {
            var current = jQuery(this),
                container = jQuery('.slide-wrapper'),
                elements = current.find('.thumb-item'),
                next = container.find('#slide_next'),
                prev = container.find('#slide_prev'),
                nav = container.find('.slide-nav'),
                width = 100,
                count = elements.length;
            slide_count = 3;

            if (count > 3) {
                next.click(function () {
                    current.stop(true, true);
                    current.animate({left: '-=100'}, 'normal', function () {
                        var elements = container.find('.thumb-item'),
                            first = elements.first(),
                            left = current.offset().left;
                        current.append(first);
                        current.offset({left: left + 100});
                    });
                    return false;
                })

                prev.click(function () {
                    var elements = container.find('.thumb-item'),
                        last = elements.last(),
                        left = current.offset().left;
                    current.prepend(last);
                    current.offset({left: left - 100});
                    current.stop(true, true);
                    current.animate({left: '+=100'}, 'normal', function () {
                    });
                    return false;
                })
            }
            else {
                nav.hide();
            }
        });
        var images_url = '<?php echo get_bloginfo('template_url') . '/images/' ?>';
        var lightbox_args = {
            imageLoading: images_url + 'lightbox-ico-loading.gif',
            imageBtnClose: images_url + 'lightbox-btn-close.gif',
            imageBtnPrev: images_url + 'lightbox-btn-prev.gif',
            imageBtnNext: images_url + 'lightbox-btn-next.gif',
            imageBlank: images_url + 'lightbox-blank.gif'
        }

        jQuery('a.product-big-photo').lightBox(lightbox_args);

    });


    function tgt_show_image(id) {
        var count_images = <?php if(!empty($product_images)) echo count($product_images); else echo 0; ?>;
        for (var i = 0; i < count_images; i++) {
            document.getElementById('big_image_' + i).style.display = "none";
        }
        document.getElementById('big_image_' + id).style.display = '';
    }
</script>
<?php
get_footer();
?>
<?php
function count_view_product($post_id)
{
    $cookie_views = array();
    $cookie_views = isset ($_COOKIE['view_product']) ? $_COOKIE['view_product'] : '';
    $count = 1;
    if (!empty($cookie_views)) {
        $cookie_views = explode(',', $cookie_views);
        foreach ($cookie_views as $cookie_view) {
            if ($cookie_view == $post_id) {
                $count = 0;
            }
        }
    }
    if ($count == 1) {
        $cookie_views[] = $post_id;
        $cookie_views = implode(',', $cookie_views);
        setcookie("view_product", $cookie_views, time() + 2 * 3600, "/");
    }
    $view_post = 0;
    $view_post = get_post_meta($post_id, PRODUCT_VIEW_COUNT, true);
    $view_post += $count;
    update_post_meta($post_id, PRODUCT_VIEW_COUNT, $view_post);
}
?>