msgid ""
msgstr ""
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Text in function
#: mainstream/functions/admin-functions.php:1225
msgid " Archive"
msgstr ""

#. Text in function
#: mainstream/archive.php:24
#: mainstream/single.php:9
#: mainstream/search.php:11
#: mainstream/index.php:38
msgid "% Comments"
msgstr ""

#. Text in function
#: mainstream/comments.php:19
msgid "% Responses"
msgstr ""

#. Text in function
#: mainstream/comments.php:78
#: mainstream/comments.php:81
#: mainstream/comments-legacy.php:141
#: mainstream/comments-legacy.php:144
msgid "(required)"
msgstr ""

#. Text in function
#: mainstream/archive.php:24
#: mainstream/single.php:9
#: mainstream/search.php:11
#: mainstream/index.php:38
msgid "0 Comments"
msgstr ""

#. Text in function
#: mainstream/archive.php:24
#: mainstream/single.php:9
#: mainstream/search.php:11
#: mainstream/index.php:38
msgid "1 Comment"
msgstr ""

#. Text in echo
#: mainstream/index.php:21
msgid "ALL POSTS"
msgstr ""

#. Text in echo
#: mainstream/functions/admin-custom-nav.php:2370
msgid "Advanced Options:"
msgstr ""

#. Text in echo
#: mainstream/archive.php:7
#: mainstream/archive.php:10
#: mainstream/archive.php:13
#: mainstream/archive.php:16
msgid "Archive"
msgstr ""

#. Text in echo
#: mainstream/template-archives.php:13
msgid "Archives"
msgstr ""

#. Text in function
#: mainstream/functions/admin-functions.php:1225
msgid "Author Archives"
msgstr ""

#. Text in echo
#: mainstream/index.php:23
msgid "BE MY FRIEND ON FACEBOOK:"
msgstr ""

#. Text in echo
#: mainstream/template-sitemap.php:21
msgid "Blog / News Categories"
msgstr ""

#. Text in echo
#: mainstream/template-sitemap.php:25
msgid "Blog / News Monthly Archives"
msgstr ""

#. Text in echo
#: mainstream/comments.php:48
msgid "Comments are closed."
msgstr ""

#. Text in echo
#: mainstream/template-archives.php:27
msgid "Categories"
msgstr ""

#. Text in function
#: mainstream/functions/admin-functions.php:1225
msgid "Category Archive"
msgstr ""

#. Text in echo
#: mainstream/comments-legacy.php:37
msgid "Comments For This Post"
msgstr ""

#. Text in echo
#: mainstream/comments-legacy.php:117
msgid "Comments are closed."
msgstr ""

#. Text in echo
#: mainstream/archive.php:43
#: mainstream/search.php:30
msgid "Continue Reading"
msgstr ""

#. Text in function
#: mainstream/functions/admin-functions.php:1225
msgid "Daily Archive"
msgstr ""

#. Text in echo
#: mainstream/includes/theme-comments.php:25
msgid "Direct link to this comment"
msgstr ""

#. Text in echo
#: mainstream/template-fullwidth.php:29
#: mainstream/page.php:23
#: mainstream/archive.php:59
#: mainstream/404.php:7
#: mainstream/single.php:35
#: mainstream/search.php:46
msgid "Error 404"
msgstr ""

#. Text in echo
#: mainstream/index.php:22
msgid "FOLLOW ME ON TWITTER:"
msgstr ""

#. Text in echo
#: mainstream/includes/theme-widgets.php:14
msgid "Flickr Photos"
msgstr ""

#. Text in function
#: mainstream/functions/admin-medialibrary-uploader.php:383
msgid "Gallery"
msgstr ""

#. Text in echo
#: mainstream/index.php:7
msgid "Hi! Welcome..."
msgstr ""

#. Text in echo
#: mainstream/comments-legacy.php:126
msgid "Leave a Reply"
msgstr ""

#. Text in echo
#: mainstream/comments.php:73
#: mainstream/comments-legacy.php:136
msgid "Logged in as"
msgstr ""

#. Text in echo
#: mainstream/comments.php:73
#: mainstream/comments-legacy.php:136
msgid "Logout &raquo;"
msgstr ""

#. Text in echo
#: mainstream/comments.php:81
#: mainstream/comments-legacy.php:144
msgid "Mail (will not be published)"
msgstr ""

#. Text in function
#: mainstream/functions/admin-functions.php:1225
msgid "Monthly Archive"
msgstr ""

#. Text in echo
#: mainstream/template-archives.php:33
msgid "Monthly Archives"
msgstr ""

#. Text in echo
#: mainstream/comments.php:78
#: mainstream/comments-legacy.php:141
msgid "Name"
msgstr ""

#. Text in function
#: mainstream/comments.php:19
msgid "No Responses"
msgstr ""

#. Text in function
#: mainstream/comments.php:19
msgid "One Response"
msgstr ""

#. Text in echo
#: mainstream/template-sitemap.php:17
msgid "Pages"
msgstr ""

#. Text in function
#: mainstream/functions/admin-medialibrary-uploader.php:383
msgid "Previously Uploaded"
msgstr ""

#. Text in echo
#: mainstream/archive.php:43
msgid "Read the full entry"
msgstr ""

#. Text in echo
#: mainstream/index.php:21
msgid "SUBSCRIBE TO OUR FEEDS:"
msgstr ""

#. Text in echo
#: mainstream/footer.php:32
msgid "Say something clever here. Or not. You decide."
msgstr ""

#. Text in echo
#: mainstream/comments-legacy.php:87
msgid "Says:"
msgstr ""

#. Text in echo
#: mainstream/search-form.php:3
msgid "Search"
msgstr ""

#. Text in function
#: mainstream/functions/admin-functions.php:1225
msgid "Search Results"
msgstr ""

#. Text in echo
#: mainstream/search.php:5
msgid "Search results for"
msgstr ""

#. Text in echo
#: mainstream/search-form.php:7
#: mainstream/search-form.php:8
msgid "Search..."
msgstr ""

#. Text in echo
#: mainstream/functions/admin-custom-nav.php:2335
msgid "Select Menu:"
msgstr ""

#. Text in echo
#: mainstream/functions/admin-custom-nav.php:2418
msgid "Show Top Level Descriptions:"
msgstr ""

#. Text in echo
#: mainstream/template-sitemap.php:13
msgid "Sitemap"
msgstr ""

#. Text in echo
#: mainstream/includes/theme-widgets.php:69
msgid "Sponsors"
msgstr ""

#. Text in echo
#: mainstream/comments.php:92
#: mainstream/comments-legacy.php:155
msgid "Submit Comment"
msgstr ""

#. Text in function
#: mainstream/functions/admin-functions.php:1225
msgid "Tag Archive"
msgstr ""

#. Text in echo
#: mainstream/functions/admin-custom-nav.php:2438
msgid "The Custom Menu has not been configured correctly.  Please check your theme settings before adding this widget."
msgstr ""

#. Text in echo
#: mainstream/template-archives.php:17
msgid "The Last 30 Posts"
msgstr ""

#. Text in echo
#: mainstream/template-fullwidth.php:33
#: mainstream/page.php:27
#: mainstream/archive.php:63
#: mainstream/404.php:11
#: mainstream/single.php:39
#: mainstream/search.php:50
#: mainstream/index.php:75
msgid "The page you are looking for does not exist. Please check the URL for typing errors, or"
msgstr ""

#. Text in echo
#: mainstream/comments.php:7
#: mainstream/comments-legacy.php:9
msgid "This post is password protected. Enter the password to view comments."
msgstr ""

#. Text in echo
#: mainstream/functions/admin-custom-nav.php:2361
msgid "Title:"
msgstr ""

#. Text in echo
#: mainstream/comments-legacy.php:79
msgid "Trackbacks For This Post"
msgstr ""

#. Text in echo
#: mainstream/comments.php:33
msgid "Trackbacks/Pingbacks"
msgstr ""

#. Text in echo
#: mainstream/comments.php:84
#: mainstream/comments-legacy.php:147
msgid "Website"
msgstr ""

#. Text in function
#: mainstream/functions/admin-custom-nav.php:1994
msgid "Woo - Custom Nav Menu"
msgstr ""

#. Text in echo
#: mainstream/footer.php:34
msgid "WooThemes"
msgstr ""

#. Text in echo
#: mainstream/footer.php:34
msgid "WooThemes Logo"
msgstr ""

#. Text in echo
#: mainstream/functions/admin-custom-nav.php:2390
msgid "Wrap in container DIV:"
msgstr ""

#. Text in echo
#: mainstream/functions/admin-custom-nav.php:2403
msgid "Wrap in container UL:"
msgstr ""

#. Text in function
#: mainstream/functions/admin-functions.php:1225
msgid "Yearly Archive"
msgstr ""

#. Text in echo
#: mainstream/comments.php:66
#: mainstream/comments-legacy.php:129
msgid "You must be"
msgstr ""

#. Text in echo
#: mainstream/index.php:11
msgid "You need to add content in the \"About You\" field on the Mainstream Options tab in the WP backend."
msgstr ""

#. Text in function
#: mainstream/includes/theme-comments.php:38
#: mainstream/comments-legacy.php:57
#: mainstream/comments-legacy.php:89
msgid "Your comment is awaiting moderation."
msgstr ""

#. Text in echo
#: mainstream/template-fullwidth.php:33
#: mainstream/page.php:27
#: mainstream/archive.php:63
#: mainstream/404.php:11
#: mainstream/single.php:39
#: mainstream/search.php:50
#: mainstream/index.php:75
msgid "and start over"
msgstr ""

#. Text in echo
#: mainstream/comments-legacy.php:61
msgid "at"
msgstr ""

#. Text in echo
#: mainstream/template-archives.php:23
msgid "comments"
msgstr ""

#. Text in echo
#: mainstream/template-fullwidth.php:33
#: mainstream/page.php:27
#: mainstream/archive.php:63
#: mainstream/404.php:11
#: mainstream/single.php:39
#: mainstream/search.php:50
#: mainstream/index.php:75
msgid "head back home"
msgstr ""

#. Text in echo
#: mainstream/comments.php:66
#: mainstream/comments-legacy.php:129
msgid "logged in"
msgstr ""

#. Text in echo
#: mainstream/comments.php:66
#: mainstream/comments-legacy.php:129
msgid "to post a comment."
msgstr ""
