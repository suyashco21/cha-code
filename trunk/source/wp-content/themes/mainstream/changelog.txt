*** Mainstream Changelog ***

2012.01.22 - version 1.8
 * index.php, archive.php, search.php - replaced wp_pagenavi() with woo_pagination()
 * style.css and /styles/ - updated .Nav styling

2011.11.11 - version 1.7.2
 * style.css - Increase line-height of .post h2.title from 20px to 30px to prevent vertical bunching of titles that span two or more lines.

2011.01.20 - version 1.7.1
 * index.php - added stripslashes to about section.

2010.11.11 - version 1.7
 * includes/theme-options.php - updated options
 * index.php - added option for welcome message
 * /functions/ - updated framework 3.2.01

2010.06.28 - version 1.6.1
 * style.css - Fixed Gravity forms styling bug

2010.06.21 - version 1.6.0
 * header.php - Added theme support for WordPress 3.0 Menu Management
 * footer.php - Added theme support for WordPress 3.0 Menu Management
 * /includes/theme-functions.php - Added theme support for WordPress 3.0 Menu Management
 
2010.06.15 - version 1.5.2
 * style.css - Added styling for Gravity forms

2010.06.09 - version 1.5.1
 * style.css - Navigation CSS bug

2010.04.29 - version 1.5.0
 * header.php - added support for WooNav
 * footer.php - added support for WooNav
 * search.php - sanitized search
 
2010.04.20 - version 1.4
 * /functions/* - MAJOR UPDATE - Framework V.2.7.0
 * header.php - Added SEO tags, woo_title(); & woo_meta();
 * functions.php - Changed layout for loading required files.

2009.09.17 - version 1.3.1
 * /functions/* - Core Framework 1.1
 * /psd/* - Added Photoshop file

2009.08.30 - version 1.3
 * 404.php - Corrected styling.
 * /functions/* - Core Framework 1.0.7
 * /lang/mainstream.po - Added language file. See: http://www.woothemes.com/2009/08/how-to-translate-a-theme/
 * Added gettext functions to template php filesdate_default_timezone_get()support localization.

2009.08.13 - version 1.2
  * /functions/ - Core Framework Update to V.1.0.6

2009.07.28 - version 1.1
  	* /functions/ - Core framework upgrade to V.1.0.4
  	* /includes/theme-options.php - Added Custom CSS backend option
  	* /includes/theme-functions.php - Removed duplicate get_page_id() function

2009.07.28 - version 1.0.1
	* /includes/theme-options.php - Added options to toggle between the_content & the_excerpt on the home & archive pages
	* index.php - Added functionality to toggle between the_content & the_excerpt for posts
	  archive.php
	  search.php

2009.07.23 - version 1.0.0
	* First release!