			<div class="clear"></div>
		
	</div><!-- /#inside -->
	</div><!-- /#main -->
	
	<div id="footer">
		<?php
		if ( function_exists('has_nav_menu') && has_nav_menu('footer-menu') ) {
			wp_nav_menu( array( 'depth' => 1, 'sort_column' => 'menu_order', 'container' => 'ul', 'theme_location' => 'footer-menu' ) );
		} else {
		?>
		<ul>
			<?php 
            if ( get_option('woo_custom_nav_menu') == 'true' ) {
            	if ( function_exists('woo_custom_navigation_output') )
                	woo_custom_navigation_output('name=Woo Menu 2');
    
            } else { ?>
			<?php if (is_page()) { $highlight = "page_item"; } else {$highlight = "page_item current_page_item"; } ?>
				
			<li class="<?php echo $highlight; ?>"><a href="<?php bloginfo('url'); ?>">Home</a></li>
			
			<?php 
				if (get_option('woo_foot_cat_menu') == 'true') 
					wp_list_categories('sort_column=menu_order&depth=1&title_li=&exclude='.get_option('woo_foot_nav_exclude')); 
				else
					wp_list_pages('sort_column=menu_order&depth=1&title_li=&exclude='.get_option('woo_foot_nav_exclude')); 
			?>
			<?php } ?>
		</ul>
		<?php } ?>
		<p>&copy; <?php bloginfo('title'); ?>. <?php _e('Say something clever here. Or not. You decide.', 'woothemes'); ?></p>
		
		<p><a href="http://woothemes.com" title="<?php _e('WooThemes', 'woothemes'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/woothemes.png" alt="<?php _e('WooThemes Logo', 'woothemes'); ?>" /></a></p>
	
	</div><!-- /#footer -->

</div><!-- /#container -->


<?php wp_footer(); ?>

</body>
</html>