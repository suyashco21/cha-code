<?php
if(isset($_POST['register_user']))
{
    $data = array();
    $data['username'] = $_POST['username'];
    $data['password'] = $_POST['password'];
    $data['confirm_pass'] = $_POST['confirm_password'];
    $data['user_email'] = $_POST['user_email'];
    //$data['profile_name'] = stripslashes(strip_tags( $_POST['profile_name'] ));
    $data['profile_name'] = $_POST['username'];
    $data['security_code'] = $_POST['sc'];
    $error = array();
    $setting_enable_captchar = get_option(SETTING_ENABLE_CAPTCHA);
    if($setting_enable_captchar['register'])
    {
        require_once( TEMPLATEPATH . '/lib/captchahelper.php' ) 	;
        $captchaHelper = new CaptchaHelper();
        $validateResult = $captchaHelper->validate();

        if ( !$validateResult['success'] ){
            $error[] = __ ('Error: Security code is incorrect.', 're');
        }
    }
    if (empty($data['password']))
    {
        $error[] = __ ('Error: Password can\'t be empty.', 're');
    }
    elseif($data['password'] != $data['confirm_pass'])
    {
        $error[] = __ ('Error: Confirm password is invalid.', 're');
    }
    elseif (empty($error))
    {
        $errors = register_new_user($data['username'], $data['user_email'], $data['password']);
        if ( is_wp_error($errors) ) {
            foreach($errors->errors as $err){
                $error[]=$err[0];
            }
        }
    }

    $user = &$user_name;

    $secure_cookie = '';

    // If the user wants ssl but the session is not ssl, force a secure cookie.
    if ( !empty($_POST['log']) && !force_ssl_admin() ) {
        $user_name = sanitize_user($_POST['log']);
        if ( $user = get_user_by('login', $user_name) ) {
            if ( get_user_option('use_ssl', $user->ID) ) {
                $secure_cookie = true;
                force_ssl_admin(true);
            }
        }
    }

    if ( isset( $_REQUEST['redirect_to'] ) ) {
        $redirect_to = $_REQUEST['redirect_to'];
        // Redirect to https if user wants ssl
        if ( $secure_cookie && false !== strpos($redirect_to, 'wp-admin') )
            $redirect_to = preg_replace('|^http://|', 'https://', $redirect_to);
    } else {
        $redirect_to = admin_url();
    }

    $reauth = empty($_REQUEST['reauth']) ? false : true;

    // If the user was redirected to a secure login form from a non-secure admin page, and secure login is required but secure admin is not, then don't use a secure
    // cookie and redirect back to the referring non-secure admin page. This allows logins to always be POSTed over SSL while allowing the user to choose visiting
    // the admin via http or https.
    if ( !$secure_cookie && is_ssl() && force_ssl_login() && !force_ssl_admin() && ( 0 !== strpos($redirect_to, 'https') ) && ( 0 === strpos($redirect_to, 'http') ) )
        $secure_cookie = false;

    $redirect_to = apply_filters('login_redirect', $redirect_to, isset( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : '', $user);

    if ( ( empty( $redirect_to ) || $redirect_to == 'wp-admin/' || $redirect_to == admin_url() ) ) {
        // If the user doesn't belong to a blog, send them to user admin. If the user can't edit posts, send them to their profile.
        if ( is_multisite() && !get_active_blog_for_user($user->ID) && !is_super_admin( $user->ID ) )
            $redirect_to = user_admin_url();
        elseif ( is_multisite() && !$user->has_cap('read') )
            $redirect_to = get_dashboard_url( $user->ID );
        elseif ( !$user->has_cap('edit_posts') )
            $redirect_to = admin_url('profile.php');
    }
    wp_safe_redirect($redirect_to);
    exit();
}


function register_new_user( $user_login, $user_email, $password ) {
    $errors = new WP_Error();

    $sanitized_user_login = sanitize_user( $user_login );
    $user_email = apply_filters( 'user_registration_email', $user_email );

    // Check the username
    if ( $sanitized_user_login == '' ) {
        $errors->add( 'empty_username', __( '<strong>ERROR</strong>: Please enter a username.' ) );
    } elseif ( ! validate_username( $user_login ) ) {
        $errors->add( 'invalid_username', __( '<strong>ERROR</strong>: This username is invalid because it uses illegal characters. Please enter a valid username.' ) );
        $sanitized_user_login = '';
    } elseif ( username_exists( $sanitized_user_login ) ) {
        $errors->add( 'username_exists', __( '<strong>ERROR</strong>: This username is already registered, please choose another one.' ) );
    }

    // Check the e-mail address
    if ( $user_email == '' ) {
        $errors->add( 'empty_email', __( '<strong>ERROR</strong>: Please type your e-mail address.' ) );
    } elseif ( ! is_email( $user_email ) ) {
        $errors->add( 'invalid_email', __( '<strong>ERROR</strong>: The email address isn&#8217;t correct.' ) );
        $user_email = '';
    } elseif ( email_exists( $user_email ) ) {
        $errors->add( 'email_exists', __( '<strong>ERROR</strong>: This email is already registered, please choose another one.' ) );
    }

    do_action( 'register_post', $sanitized_user_login, $user_email, $errors );

    $errors = apply_filters( 'registration_errors', $errors, $sanitized_user_login, $user_email );

    if ( $errors->get_error_code() )
        return $errors;

    $user_id = wp_create_user( $sanitized_user_login, $password, $user_email );
    if ( ! $user_id ) {
        $errors->add( 'registerfail', sprintf( __( '<strong>ERROR</strong>: Couldn&#8217;t register you... please contact the <a href="mailto:%s">webmaster</a> !' ), get_option( 'admin_email' ) ) );
        return $errors;
    }

    wp_new_user_notification( $user_id, $password );

    wp_set_auth_cookie( $user_id, false, is_ssl() );

    return $user_id;
}
	