<?php 
global $helper,$wp_query,$posts,$cat,$wpdb,$post;

$wp_query = new WP_Query();

add_action('posts_where', 'where_article_of_product');
add_action('posts_join', 'join_article_of_product' );
add_action('post_limits', 'article_of_product_limits' );
add_action('posts_orderby', 'order_article_of_product');

$wp_query->get_posts();

?>
	<?php													
	if ( have_posts() ) 
	{
echo '<div class="content_articles">';
		while(have_posts())
		{
			the_post();
			$art_id = get_the_ID();

	?>
<div class="articlebox">
	<div style="padding: 0 20px">
		<div>
			<div style="margin-top: 10px;">

					<a href="<?php the_permalink(); ?>"><h4 style="display:inline;"><?php echo $post->post_title ?></h4></a>

                <span class="widget-stat-time" style="display:inline;float:right;"> <?php echo the_time(get_option('date_format')); ?> </span>
			</div>
		    <hr/>
			<div>
				<p><?php echo get_the_content() ?></p>
				<br/>
				
				<p><a style="color:#1793D3;" href="<?php the_permalink(); ?>"><?php _e('Comments','re'); ?>  (<?php echo $post->comment_count; ?>)</a></p>
			</div>
		</div>
	</div>
</div>
<?php
				}
echo '</div>';
	}
	else {?>
		
		<div class="content_review">
			<div class="revieww" style="padding-bottom: 20px;">
				<p class="red" style="margin: 10px 20px"> <?php _e('This product has no article yet','re'); ?> </p>
			</div>
		</div>
	<?php
	}	
?>