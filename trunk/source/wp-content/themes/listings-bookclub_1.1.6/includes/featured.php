<div id="featured_slider" class="featured_slider jCarousel">
	
	<?php if(get_option('woo_featured_header')) { ?><h2 class="cufon"><?php echo stripslashes(get_option('woo_featured_header')); ?></h2><?php } ?>
    <?php $woo_featured_tags = get_option('woo_featured_tags'); if ( ($woo_featured_tags != '') && (isset($woo_featured_tags)) ) { ?>
    <?php $woo_slider_pos = get_option('woo_slider_image'); ?>
    <?php
		$featposts = get_option('woo_featured_entries'); // Number of featured entries to be shown
		$GLOBALS['feat_tags_array'] = explode(',',get_option('woo_featured_tags')); // Tags to be shown
        foreach ($GLOBALS['feat_tags_array'] as $tags){ 
			$tag = get_term_by( 'name', trim($tags), 'post_tag', 'ARRAY_A' );
			if ( $tag['term_id'] > 0 )
				$tag_array[] = $tag['term_id'];
		}
    ?>
	<?php $saved = $wp_query; query_posts(array('post_type' => 'any','tag__in' => $tag_array, 'showposts' => $featposts)); ?>
    <?php if (have_posts() && ($featposts > 1)) : $count = 0; ?>
	<?php /* Temporarily commented out nav buttons in favour of those generated by jCarousel. - 2011-01-04. */ ?>
	<?php /*
	<ul class="nav-buttons <?php if ($woo_slider_pos == 'Right') { echo 'right'; } ?>">
    	<li id="n"><a href="#" class="next"></a></li>
        <li id="p"><a href="#" class="previous"></a></li>
    </ul>
	*/ ?>        
	<?php endif; $wp_query = $saved; ?>      

	<?php $saved = $wp_query; query_posts(array('post_type' => 'any','tag__in' => $tag_array, 'showposts' => $featposts)); ?>
	<?php if (have_posts()) : $count = 0; ?>
	
    <div class="container">
    <ul id="jcarousel-slides" class="slides"> 
    	<?php while (have_posts()) : the_post(); $GLOBALS['shownposts'][$count] = $post->ID; $count++; ?>
        <?php 
    	    global $post;
    	    $post_id = $post->ID;
    	    $post_type = $post->post_type;
    	    //Meta Data
    	    $custom_field = $woo_options['woo_slider_image_caption'];
    	    $listing_image_caption = get_post_meta($post->ID,$custom_field,true);
    	    $custom_fields = get_post_custom( $post_item->ID );
			if ($listing_image_caption != '' && $custom_field == 'price') { $listing_image_caption = number_format($listing_image_caption , 0 , '.', ','); }
    	?>
    	<li id="slide-<?php echo $count; ?>" class="slide">
            	
            	<div class="slider-img <?php if ($woo_slider_pos == 'Right') { echo 'fr'; } else { echo 'fl'; } ?>">
            		
            		<?php
            		// If a featured image is available, use it in priority over the "image" field.
					if ( function_exists( 'has_post_thumbnail' ) && current_theme_supports( 'post-thumbnails' ) ) {
					
						if ( has_post_thumbnail( $post_id ) ) {
						
							$_id = 0;
							$_id = get_post_thumbnail_id( $post_id );
							
							if ( intval( $_id ) ) {
							
								$_image = array();
								$_image = wp_get_attachment_image_src( $_id, 'full' );
								
								// $_image should have 3 indexes: url, width and height.
								if ( count( $_image ) ) {
								
									$_image_url = $_image[0];
									
									woo_image('src=' . $_image_url . '&key=image&width=115&height=178&link=img');
												
								} // End IF Statement
							
							} // End IF Statement
						
						} else {
							
							woo_image('id='.$post_id.'&key=image&width=115&height=178&link=img');
						
						} // End IF Statement
					
					} else {
						
						woo_image('id='.$post_id.'&key=image&width=115&height=178&link=img');
					
					} // End IF Statement
            		
            		// woo_get_image('image',115,178,' '.get_option('woo_slider_image'));
            		?>
            		
            		<a href="<?php echo get_permalink($post_item->ID); ?>">
            		
	            		<span class="rollover">
	            		
		            		<span class="title"><?php echo get_the_title($post_item->ID); ?></span>
		        			<span class="author"><em><?php _e('by', 'woothemes'); ?></em><strong>
		        			<?php
								$authors = get_the_term_list( $post_item->ID, 'book_authors', ' ', ', ', '' );
								
								if ( ! is_wp_error( $authors ) ) { echo strip_tags($authors); } // End IF Statement
							?>
							</strong></span>
			
		        			<span class="learn-more"><?php _e('Learn more', 'woothemes'); ?></span>
		        			
		        		</span>
            	
            		</a>
            		
            	</div>
	
            	<div class="fix"></div>
            	        
            </li>
    	<?php endwhile; ?> 
    
	</ul>
	      
    </div><!-- /.container -->
	<div class="fix"></div>
    
    <?php endif; $wp_query = $saved; ?>
    <?php
    	wp_reset_query();
    ?>
    <?php if (get_option('woo_exclude') <> $GLOBALS['shownposts']) update_option("woo_exclude", $GLOBALS['shownposts']); ?>
    <?php } else { ?>    
	<p class="note"><?php _e('Please setup Featured Panel tag(s) in your options panel. You must setup tags that are used on active posts.','woothemes'); ?></p>
	<?php } ?>
</div><!-- /#loopedSlider -->
