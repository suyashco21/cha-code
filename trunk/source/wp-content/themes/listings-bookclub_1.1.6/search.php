<?php ob_start(); ?>
<?php get_header(); ?>
<?php 
$query_args = array();
// Get Custom Search Data
$search_data = woo_dynamic_search_header();
$query_args = $search_data['query_args'];
$array_counter = $search_data['array_counter'];
$has_results = $search_data['has_results'];
$blog_search = $search_data['blog_search'];
// Handle WebRef
if ( $array_counter == 1 && isset($_GET['listings-search-webref-submit'])) { $location_header = get_permalink( $keyword_to_search_sanitized ); header("Location: ".$location_header); }
// Output to the browser
ob_flush();
query_posts($query_args);
?>

    <?php include ( TEMPLATEPATH . '/search-form.php' ); ?>  
    <div id="content" class="col-full">
		<div id="main" class="fullwidth">
            <?php
            	$_more = false;
            	
            	if ( isset( $_REQUEST['more'] ) ) {
            	
            		$_moretext = strtolower( trim( strip_tags( $_REQUEST['more'] ) ) );
            	
            		if ( $_moretext == 'yes' ) {
            		
            			$_more = true;
            		
            		} // End IF Statement
            	
            	} // End IF Statement
            	
            	if ( $_more ) {
            	
            	$_text = __( 'More listings', 'woothemes' );
            	
            	if ( array_key_exists( 'woo_listings_more_header', $woo_options ) && $woo_options['woo_listings_more_header'] != '' ) {
				
					$_text = $woo_options['woo_listings_more_header'];
				
				} // End IF Statement
            	
            ?>
            <span class="archive_header"><?php echo $_text; ?></span>
            <?php
            	
            	} else {
            	
            ?>
            <span class="archive_header cufon"><?php _e($woo_options['woo_search_results_header'], 'woothemes') ?> <?php if ($keyword_to_search != '') { _e('for', 'woothemes'); ?> <em><?php echo '"'.$keyword_to_search.'"'; ?></em><?php } ?></span>
            <?php
            	
            	} // End IF Statement
            	
            ?>
            <?php if ($has_results || $blog_search || $_more) { ?>
			<?php if ( function_exists('yoast_breadcrumb') ) { yoast_breadcrumb('<div id="breadcrumb"><p>','</p></div>'); } ?>
			<?php if (have_posts()) : $count = 0; ?>
            
            <div class="more-listings">
                
            <?php while (have_posts()) : the_post(); $count++; ?>
                                          
            <div class="block">

                <?php woo_image('key=image&width=95&height=147'); ?>			    	
        	
	        	<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
	        	
	        	<?php
					$authors = get_the_term_list( $post_item->ID, 'book_authors', '<p class="author">by <span> ', ', ', '</span></p>' );
					
					if ( ! is_wp_error( $authors ) ) { echo $authors; } // End IF Statement
				?>
						
	        	<span class="more"><a href="<?php echo get_permalink($post_item->ID); ?>" title="<?php echo get_the_title($post_item->ID); ?>"><?php _e('Learn more', 'woothemes'); ?></a></span>
        	
            </div><!-- /.block -->
            
            <?php
            	if ( $count % 3 == 0 ) {
            ?>
            	<div class="fix"></div>
            <?php
            	} // End IF Statement
            ?>
            
                                                 
            <?php endwhile; ?>
            
            </div><!-- /.more-listings -->
            
            <div class="fix"></div>
            
            <?php else: ?>
            
                <div class="post">
                    <p class="woo-sc-box note"><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
                </div><!-- /.post -->

            <?php endif; ?>  
        		
        	<?php if ($has_results) { ?> 
        		<?php woo_listingsnav(); ?>
        	 <?php } ?> 
        	 
            <?php } else { ?>
            	<div class="fix"></div>
            	
            	<div class="post">
                    <p class="woo-sc-box note"><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
                </div><!-- /.post -->
                
            <?php } ?>    
        </div><!-- /#main -->

    </div><!-- /#content -->

	
<?php get_footer(); ?>
