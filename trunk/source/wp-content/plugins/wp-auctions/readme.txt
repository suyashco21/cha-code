WP Auctions Pro, Instant Download
---------------------------------

Version: 4.4.1

WP Auctions helps you to host and manage personal auctions on your own WordPress powered blog.

Keep checking our blog to know when new versions are released.

Please report any problems with the plugin at http://www.wpauctions.com/forums/


Localisation support
--------------------

WP Auctions will use the localisation options specified in your WordPress configuration file.

For more information on how to switch WordPress to your lanugage read - http://codex.wordpress.org/Installing_WordPress_in_Your_Language

If you would like to help translate WP Auctions into your own langauge, check out our blog post on how to help - http://www.wpauctions.com/help-us-translate-wp-auctions/


