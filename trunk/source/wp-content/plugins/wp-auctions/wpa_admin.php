<?php

function wp_auctions_options() {

   global $wpdb;
      
   // Note: Options for this plugin include a "Title" setting which is only used by the widget
   $options = get_option('wp_auctions');
	
   //set initial values if none exist
   if ( !is_array($options) ) {
      $options = array( 'title'=>'WP Auctions', 'engine'=>STANDARD_ENGINE, 'otherauctions'=>'3', 'currency'=>'1', 'style'=>'default', 'notify'=>'', 'paypal'=>'', 'mailingaddress'=>'', 'bankdetails'=>'', 'currencysymbol'=>'$', 'currencycode'=>'USD','noauction'=>'','customcontact'=>'','customincrement'=>'','refreshtimeout'=>'10');
   }

   if (isset($_POST['wp_auctions-submit']) ) {

      // security check
      check_admin_referer( 'WPA-nonce');

      $options['currency'] = strip_tags(stripslashes($_POST['wpa-currency']));
      $options['engine'] = strip_tags(stripslashes($_POST['wpa-engine']));
      $options['title'] = strip_tags(stripslashes($_POST['wpa-title']));
      $options['notify'] = strip_tags(stripslashes($_POST['wpa-notify']));
      $options['paypal'] = strip_tags(stripslashes($_POST['wpa-paypal']));
      $options['mailingaddress'] = strip_tags(stripslashes($_POST['wpa-mailingaddress']));
      $options['bankdetails'] = strip_tags(stripslashes($_POST['wpa-bankdetails']));
      $options['list'] = strip_tags(stripslashes($_POST['wpa-list']));
      $options['feedback'] = strip_tags(stripslashes($_POST['wpa-feedback']));
      $options['regonly'] = strip_tags(stripslashes($_POST['wpa-regonly']));
      $options['otherauctions'] = strip_tags(stripslashes($_POST['wpa-otherauctions']));
      $options['customcontact'] = strip_tags(stripslashes($_POST['wpa-customcontact']));
      $options['noauction'] = stripslashes($_POST['wpa-noauction']); // don't strip tags
      $options['style'] = strip_tags(stripslashes($_POST['wpa-style']));
      $options['customincrement'] = (float) strip_tags(stripslashes($_POST['wpa_customincrement']));
      $options['remotedebug'] = strip_tags(stripslashes($_POST['wpa-remotedebug']));
      $options['showattrib'] = strip_tags(stripslashes($_POST['wpa-showattrib']));
      $options['showrss'] = strip_tags(stripslashes($_POST['wpa-showrss']));
      $options['landingpage'] = strip_tags(stripslashes($_POST['wpa-landingpage']));
      $options['endtrigger'] = strip_tags(stripslashes($_POST['wpa-endtrigger']));
      $options['endoffset'] = strip_tags(stripslashes($_POST['wpa-endoffset']));
      $options['countdown'] = strip_tags(stripslashes($_POST['wpa-countdown']));
      $options['bypassresizer'] = strip_tags(stripslashes($_POST['wpa-bypassresizer']));
      $options['bypassUTF8email'] = strip_tags(stripslashes($_POST['wpa-bypassUTF8email']));
      $options['refreshtimeout'] = strip_tags(stripslashes($_POST['wpa-refreshtimeout']));
      $options['whennotify'] = strip_tags(stripslashes($_POST['wpa-whennotify']));
      $options['overridestyle'] = strip_tags(stripslashes($_POST['wpa-overridestyle']));
      $options['requesttobid'] = strip_tags(stripslashes($_POST['wpa-requesttobid']));

      // check that requesttobid can only be YES it regonly is YES
      if ( $options['regonly'] == "") {
        $options['requesttobid'] = "";
      } 

      // cater for countdowndropdown
      if ( strip_tags(stripslashes($_POST['wpa-countdowndd'])) == 1 ) $options['countdown'] = 0;
      if ( strip_tags(stripslashes($_POST['wpa-countdowndd'])) == 3 ) $options['countdown'] = 999;

      // cater for overridestyle
      if ( strip_tags(stripslashes($_POST['wpa-overridestyledd'])) == 1 ) $options['overridestyle'] = "";

      // turn off end extention if offset or trigger are 0
      if ( $options['endtrigger'] == 0 || $options['endoffset'] == 0) $options['endtrigger'] = "";
 
      // make sure we clear custom offset/trigger if drop down is set to standard
      if (strip_tags(stripslashes($_POST['wpa-endincrement'])) == "1") {
         $options['endtrigger'] = "";
         $options['endoffset'] = "";
      }      

      // revert to no countdown if user specifies 0
      if ( $options['countdown'] == 0 ) $options['countdown'] = "";

      // revert to non-custom if user specifies 0
      if ( $options['customincrement'] == 0 ) $options['customincrement'] = "";

      // make sure we clear custom increment if drop down is set to standard
      if (strip_tags(stripslashes($_POST['wpa-bidincrement'])) == "1") {
         $options['customincrement'] = "";
      }
      
      // Currencies handled here
      $currencies = array(
          1 => array('code' => 'GPB', 'symbol' => '&pound;'),
          2 => array('code' => 'USD', 'symbol' => '$'),
          3 => array('code' => 'EUR', 'symbol' => '&#128;'),
          4 => array('code' => 'JPY', 'symbol' => '&yen;'),
          5 => array('code' => 'AUD', 'symbol' => 'A$'),
          6 => array('code' => 'CAD', 'symbol' => 'C$'),
          7 => array('code' => 'NZD', 'symbol' => 'NZ$;'),
          8 => array('code' => 'CHF', 'symbol' => 'Fr'),
          9 => array('code' => 'SGD', 'symbol' => 'S$'),
         10 => array('code' => 'BRL', 'symbol' => 'R$'),
         11 => array('code' => 'CZK', 'symbol' => 'Kc'),
         12 => array('code' => 'DKK', 'symbol' => 'kr'),
         13 => array('code' => 'HKD', 'symbol' => 'HK$'),
         14 => array('code' => 'HUF', 'symbol' => 'Ft'),
         15 => array('code' => 'ILS', 'symbol' => 'ILS'),
         16 => array('code' => 'MYR', 'symbol' => 'RM'),
         17 => array('code' => 'MXN', 'symbol' => '$'),
         18 => array('code' => 'NOL', 'symbol' => 'kr'),
         19 => array('code' => 'PHP', 'symbol' => 'PHP'),
         20 => array('code' => 'PLN', 'symbol' => 'zl'),
         21 => array('code' => 'SEK', 'symbol' => 'kr'),
         22 => array('code' => 'TWD', 'symbol' => 'NT$'),
         23 => array('code' => 'THB', 'symbol' => 'THB'),
         24 => array('code' => 'TRY', 'symbol' => 'TRY'),
      );

      if ($options['currency']!=99) {
         $options['currencysymbol'] = $currencies[$options['currency']]['symbol'];
         $options['currencycode'] = $currencies[$options['currency']]['code'];      
      } else {
         $options['currencysymbol']=strip_tags(stripslashes($_POST['wpa-currencysymbol']));;
         $options['currencycode']=strip_tags(stripslashes($_POST['wpa-currencycode']));;
      }

      update_option('wp_auctions', $options);
   }

   $currencysymbol = htmlspecialchars($options['currencysymbol'], ENT_QUOTES);
   $currencycode = htmlspecialchars($options['currencycode'], ENT_QUOTES);

   $currency = htmlspecialchars($options['currency'], ENT_QUOTES);
   $title = htmlspecialchars($options['title'], ENT_QUOTES);
   $engine = htmlspecialchars($options['engine'], ENT_QUOTES);
   $notify = htmlspecialchars($options['notify'], ENT_QUOTES);
   $paypal = htmlspecialchars($options['paypal'], ENT_QUOTES);
   $mailingaddress = htmlspecialchars($options['mailingaddress'], ENT_QUOTES);
   $bankdetails = htmlspecialchars($options['bankdetails'], ENT_QUOTES);
   $list = htmlspecialchars($options['list'], ENT_QUOTES);
   $feedback = htmlspecialchars($options['feedback'], ENT_QUOTES);
   $noauction = htmlspecialchars($options['noauction'], ENT_QUOTES);
   $regonly = htmlspecialchars($options['regonly'], ENT_QUOTES);
   $otherauctions = htmlspecialchars($options['otherauctions'], ENT_QUOTES);
   $customcontact = htmlspecialchars($options['customcontact'], ENT_QUOTES);
   $style = htmlspecialchars($options['style'], ENT_QUOTES);
   $customincrement = htmlspecialchars($options['customincrement'], ENT_QUOTES);
   $remotedebug = htmlspecialchars($options['remotedebug'], ENT_QUOTES);
   $showattrib = htmlspecialchars($options['showattrib'], ENT_QUOTES);
   $showrss = htmlspecialchars($options['showrss'], ENT_QUOTES);
	 $landingpage = htmlspecialchars($options['landingpage'], ENT_QUOTES);
   $endtrigger = htmlspecialchars($options['endtrigger'], ENT_QUOTES);
	 $endoffset = htmlspecialchars($options['endoffset'], ENT_QUOTES);
	 $countdown = htmlspecialchars($options['countdown'], ENT_QUOTES);
	 $refreshtimeout = htmlspecialchars($options['refreshtimeout'], ENT_QUOTES);
	 $bypassresizer = htmlspecialchars($options['bypassresizer'], ENT_QUOTES);
	 $bypassUTF8email = htmlspecialchars($options['bypassUTF8email'], ENT_QUOTES);
	 $whennotify = htmlspecialchars($options['whennotify'], ENT_QUOTES);
	 $overridestyle = htmlspecialchars($options['overridestyle'], ENT_QUOTES);
	 $requesttobid = htmlspecialchars($options['requesttobid'], ENT_QUOTES);
	 
	 if ($refreshtimeout == "") $refreshtimeout = 10;
	 if ($whennotify == "") $whennotify = "always";
?>

<script type="text/javascript">
function CheckCurrencyOptions() {

   var chosen=document.getElementById("wpa-currency").value;
   var WPA_activetab=document.getElementById("wpa_activetab");

   if (chosen=="99") {
      WPA_activetab.style.display = "";
   } else {
      WPA_activetab.style.display = "none";   
   }
}

function CheckIncrementOptions() {

   var chosen=document.getElementById("wpa-bidincrement").value;
   var WPA_activetab=document.getElementById("wpa_incrementtab");

   if (chosen=="2") {
      WPA_activetab.style.display = "";
   } else {
      WPA_activetab.style.display = "none";   
   }
}

function CheckCountdownOptions() {

   var chosen=document.getElementById("wpa-countdowndd").value;
   var WPA_activetab=document.getElementById("wpa_countdowntab");

   if (chosen=="2") {
      WPA_activetab.style.display = "";
   } else {
      WPA_activetab.style.display = "none";   
   }
}

function CheckOverrideStyle() {

   var chosen=document.getElementById("wpa-overridestyledd").value;
   var WPA_activetab=document.getElementById("wpa_overridestyletab");

   if (chosen=="2") {
      WPA_activetab.style.display = "";
   } else {
      WPA_activetab.style.display = "none";   
   }
}

function CheckEndIncrementOptions() {

   var chosen=document.getElementById("wpa-endincrement").value;
   var WPA_activetab=document.getElementById("wpa_endincrementtab");

   if (chosen=="2") {
      WPA_activetab.style.display = "";
   } else {
      WPA_activetab.style.display = "none";   
   }
}

function IsNumeric(sText)

{
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;

 
   for (i = 0; i < sText.length && IsNumber == true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
         IsNumber = false;
         }
      }
   return IsNumber;
   
}

function ValidateIncrement()
{

   x = document.getElementById("wpa_customincrement").value;
   
   if (!IsNumeric(x)) 
   { 
      alert('Please enter only numbers or decimal points in the account field') 
      document.getElementById("wpa_customincrement").focus(); 
      return false; 
      } 
 
return true;
 
} 

</script>

<div class="wrap"> 
  <form name="form1" method="post" action="<?php admin_url('admin.php?page='.WPA_PLUGIN_NAME); ?>">
    
  <?php wp_nonce_field('WPA-nonce'); ?>
  
  <h2 class="settings"><em><?php _e('General Settings','wpauctions') ?></em></h2>

    <table width="100%" cellspacing="2" cellpadding="5" class="widefat" style="margin-top: 1em;"> 
      <tr valign="top" class="alternate">
        <th scope="row" class='row-title'><?php _e('Show your Auctions:','wpauctions') ?></th> 
        <td class='desc'><p><?php _e('After you add your auction you can show it by using the Sidebar Widget (available under "Appearance > Widgets") or by embedding an auction in a Post/Page','wpauctions'); ?>.</p>
</td> 
      </tr> 
      <tr valign="top" class="alternate">
        <th scope="row" class='row-title'><?php _e('Auction Title:','wpauctions') ?></th> 
        <td class='desc'><input name="wpa-title" type="text" id="wpa-title" value="<?php echo $title; ?>" size="40" />
        <br />
        <p><?php _e('Enter the main header title for your auctions','wpauctions') ?></p></td> 
      </tr> 
      <tr valign="top"> 
        <th scope="row" class='row-title'><?php _e('Auction Engine:','wpauctions') ?></th> 
        <td class='desc'>        
<?php
// we can only change engine if we have no active auctions

		$table_name = $wpdb->prefix . "wpa_auctions";
		$strSQL = "SELECT count(*) FROM $table_name WHERE '".current_time('mysql',"1")."' < date_end;";
		$rows = $wpdb->get_var ($strSQL);
		
		if ($rows==0) {
?>        
        
        <select id="wpa-engine" name="wpa-engine">
                <option value="1" <?php if ($engine=='1') echo 'selected'; ?>>Standard (Proxy-Bidding)</option>
                <option value="2" <?php if ($engine=='2') echo 'selected'; ?>>Simple Bidding</option>
                <option value="3" <?php if ($engine=='3') echo 'selected'; ?>>Reverse Bidding</option>
         </select>
<?php
    } else {
       echo '<input type="hidden" name="wpa-engine" value="'.$engine.'">';
       echo "<strong>".__('This option can only be changed if there are no active auctions','wpauctions')."</strong>";
    }
?>
         
        <br />
        <p><?php _e('Which auction engine would you like to use?','wpauctions') ?></p></td> 
      </tr>       
      <tr valign="top" class="alternate"> 
        <th scope="row" class='row-title'><?php _e('Currency:','wpauctions') ?></th> 
        <td class='desc'>
        <select id="wpa-currency" name="wpa-currency" onclick="CheckCurrencyOptions()">
                <option value="1" <?php if ($currency=='1') echo 'selected'; ?>>GBP</option>
                <option value="2" <?php if ($currency=='2') echo 'selected'; ?>>USD</option>
                <option value="3" <?php if ($currency=='3') echo 'selected'; ?>>EUR</option>
                <option value="4" <?php if ($currency=='4') echo 'selected'; ?>>JPY</option>
                <option value="5" <?php if ($currency=='5') echo 'selected'; ?>>AUD</option>
                <option value="6" <?php if ($currency=='6') echo 'selected'; ?>>CAD</option>
                <option value="7" <?php if ($currency=='7') echo 'selected'; ?>>NZD</option>
                <option value="8" <?php if ($currency=='8') echo 'selected'; ?>>CHF</option>
                <option value="9" <?php if ($currency=='9') echo 'selected'; ?>>SGD</option>
                <option value="10" <?php if ($currency=='10') echo 'selected'; ?>>BRL</option>
                <option value="11" <?php if ($currency=='11') echo 'selected'; ?>>CZK</option>
                <option value="12" <?php if ($currency=='12') echo 'selected'; ?>>DKK</option>
                <option value="13" <?php if ($currency=='13') echo 'selected'; ?>>HKD</option>
                <option value="14" <?php if ($currency=='14') echo 'selected'; ?>>HUF</option>
                <option value="15" <?php if ($currency=='15') echo 'selected'; ?>>ILS</option>
                <option value="16" <?php if ($currency=='16') echo 'selected'; ?>>MYR</option>
                <option value="17" <?php if ($currency=='17') echo 'selected'; ?>>MXN</option>
                <option value="18" <?php if ($currency=='18') echo 'selected'; ?>>NOL</option>
                <option value="19" <?php if ($currency=='19') echo 'selected'; ?>>PHP</option>
                <option value="20" <?php if ($currency=='20') echo 'selected'; ?>>PLN</option>
                <option value="21" <?php if ($currency=='21') echo 'selected'; ?>>SEK</option>
                <option value="22" <?php if ($currency=='22') echo 'selected'; ?>>TWD</option>
                <option value="23" <?php if ($currency=='23') echo 'selected'; ?>>THB</option>
                <option value="24" <?php if ($currency=='24') echo 'selected'; ?>>TRY</option>
                <option value="99" <?php if ($currency=='99') echo 'selected'; ?>>Custom</option>
         </select>
        <br />
        <div id="wpa_activetab" style="display:<?php if ($currency!='99'){ echo "none"; }?>;">
          <div style="float:right; border: 2px solid red; color: #000; width: 300px;margin: -5px 10px 15px 0; padding: 5px;"><p><strong><u><?php _e('Warning!','wpauctions') ?></u></strong> <?php _e('If you use a custom currency, please remember that PayPal only supports a','wpauctions') ?> <a href="https://www.paypal.com/us/cgi-bin/webscr?cmd=p/sell/mc/mc_intro-outside"><?php _e('small set of currencies','wpauctions') ?></a>. <?php _e('If you use a currency outside this set, any PayPal payments will fail.','wpauctions') ?></p> <p><?php _e('small set of currencies','wpauctions') ?></a>. <?php _e('You can still use Bank Payments and send your Address for cheques/money orders etc...','wpauctions') ?></p></div>
          <div><?php _e('Currency Code:','wpauctions') ?> <input name="wpa-currencycode" type="text" id="wpa-currencycode" value="<?php echo $currencycode; ?>" size="5" /><br/>
          <?php _e('Currency Symbol:','wpauctions') ?> <input name="wpa-currencysymbol" type="text" id="wpa-currencysymbol" value="<?php echo $currencysymbol; ?>" size="5" /></div>
        </div>
        <p><?php _e('Choose the currency you would like to run your auctions in','wpauctions') ?></p><p><a href="http://en.wikipedia.org/wiki/List_of_circulating_currencies" target="_blank"><?php _e('Click here for custom Currency Codes and Symbols','wpauctions') ?></a>.</p></td> 
      </tr> 
      <tr valign="top"> 
        <th scope="row" class='row-title'><?php _e('Minimum Bid Increment:','wpauctions') ?></th> 
        <td class='desc'>
        <select id="wpa-bidincrement" name="wpa-bidincrement" onclick="CheckIncrementOptions()">
                <option value="1" <?php if ($customincrement=='') echo 'selected'; ?>><?php _e('Standard','wpauctions') ?></option>
                <option value="2" <?php if ($customincrement!='') echo 'selected'; ?>><?php _e('Custom','wpauctions') ?></option>
         </select>
        <br />
        <div id="wpa_incrementtab" style="display:<?php if ($customincrement==''){ echo "none"; }?>;">
          <div><?php _e('Specify minimum increment amount (Example: 10):','wpauctions') ?><br /><input name="wpa_customincrement" type="text" id="wpa_customincrement" value="<?php echo $customincrement; ?>"  onblur="ValidateIncrement()" size="5" /></div>
        </div>
        <p><?php _e('Use the standard automatic increment amounts or set a new minimum bid increment. This can be overridden on a per-auction basis. Note: This does not remove the "Maximum Bid" option bidders are allowed with Proxy bidding.','wpauctions') ?></p></td> 
      </tr> 
      <tr valign="top" class="alternate"> 
        <th scope="row" class='row-title'><?php _e('Notification:','wpauctions') ?></th> 
        <td class='desc'><input name="wpa-notify" type="text" id="wpa-notify" value="<?php echo $notify; ?>" size="40" />
        <br />
        <p><?php _e('Enter your email address if you want to be notified','wpauctions') ?>
        
        <select id="wpa-whennotify" name="wpa-whennotify">
                <option value="always" <?php if ($whennotify=='always') echo 'selected'; ?>><?php _e('whenever a new bid is placed or an auction won','wpauctions') ?></option>
                <option value="bidonly" <?php if ($whennotify=='bidonly') echo 'selected'; ?>><?php _e('whenever a new bid is placed only','wpauctions') ?></option>
                <option value="winonly" <?php if ($whennotify=='winonly') echo 'selected'; ?>><?php _e('whenever an auction is won only','wpauctions') ?></option>
         </select>
        </p></td> 
      </tr> 
      <tr valign="top"> 
        <th scope="row" class='row-title'><?php _e('Other Auctions:','wpauctions') ?></th> 
        <td class='desc'>
        <select id="wpa-otherauctions" name="wpa-otherauctions">
                <option value="1" <?php if ($otherauctions=='1') echo 'selected'; ?>>1</option>
                <option value="2" <?php if ($otherauctions=='2') echo 'selected'; ?>>2</option>
                <option value="3" <?php if ($otherauctions=='3') echo 'selected'; ?>>3</option>
                <option value="4" <?php if ($otherauctions=='4') echo 'selected'; ?>>4</option>
                <option value="5" <?php if ($otherauctions=='5') echo 'selected'; ?>>5</option>
                <option value="6" <?php if ($otherauctions=='6') echo 'selected'; ?>>6</option>
                <option value="7" <?php if ($otherauctions=='7') echo 'selected'; ?>>7</option>
                <option value="8" <?php if ($otherauctions=='8') echo 'selected'; ?>>8</option>
                <option value="9" <?php if ($otherauctions=='9') echo 'selected'; ?>>9</option>
                <option value="all" <?php if ($otherauctions=='all') echo 'selected'; ?>>All</option>
         </select>
        <br />
        <p><?php _e('How many other auctions would you like to display in the widget?','wpauctions') ?></p></td> 
      </tr> 
      <tr valign="top" class="alternate"> 
        <th scope="row" class='row-title' style="border-bottom: 0;"><?php _e('Registered Users Only Mode','wpauctions') ?></th> 
        <td class='desc' style="border-bottom: 0;">
        <select id="wpa-regonly" name="wpa-regonly">
                <option value="" <?php if ($regonly=='') echo 'selected'; ?>><?php _e('No, anyone can bid','wpauctions') ?></option>
                <option value="Yes" <?php if ($regonly=='Yes') echo 'selected'; ?>><?php _e('Yes, only registered users can bid','wpauctions') ?></option>
         </select>
        <br />
        <p><?php _e('Do bidders need to have a WordPress subscriber account to bid?','wpauctions') ?></p></td> 
      </tr> 
<?php if ($regonly=='Yes') { ?>      
      <tr valign="top""> 
        <th scope="row" class='row-title' style="border-bottom: 0;"><?php _e('Request to Bid Mode','wpauctions') ?></th> 
        <td class='desc' style="border-bottom: 0;">
        <select id="wpa-requesttobid" name="wpa-requesttobid">
                <option value="" <?php if ($requesttobid=='') echo 'selected'; ?>><?php _e('No, anyone can bid','wpauctions') ?></option>
                <option value="Yes" <?php if ($requesttobid=='Yes') echo 'selected'; ?>><?php _e('Yes, bidders need to be approved before they can bid','wpauctions') ?></option>
         </select>
        <br />
        <p><?php _e('Force users to request to bid before they can bid (required "Registered Users Only" to be turned on)','wpauctions') ?></p></td> 
      </tr> 
<?php } ?>
    </table>

    <p class="submit">
      <input type="submit" name="Submit" value="<?php _e('Update Options','wpauctions') ?> &raquo;" />
    </p>

  <h2 class="payment"><em><?php _e('Payment Settings - Please supply at least one of the following','wpauctions') ?></em></h2>

    <table width="100%" cellspacing="2" cellpadding="5" class="widefat" style="margin-top: 1em;"> 
      <tr valign="top"> 
        <th scope="row" class='row-title'><?php _e('PayPal account:','wpauctions') ?></th> 
        <td class='desc'><input name="wpa-paypal" type="text" id="wpa-paypal" value="<?php echo $paypal; ?>" size="40" />
        <br />
        <p><?php _e('Provide your PayPal email address where you want to accept payments from auction winners.','wpauctions') ?></p></td> 
      </tr> 
      <tr valign="top" class="alternate"> 
        <th scope="row" class='row-title'><?php _e('Bank Details:','wpauctions') ?></th> 
        <td class='desc'>
        <textarea rows="5" cols="100" id="wpa-bankdetails" name="wpa-bankdetails"><?php echo $bankdetails; ?></textarea>
        <br />
        <p><?php _e('Provide your bank details, this information will be sent to the auction winners so they can pay you with a wire transfer','wpauctions') ?></p></td> 
      </tr> 
      <tr valign="top"> 
        <th scope="row" class='row-title' style="border-bottom: 0;"><?php _e('Mailing Address:','wpauctions') ?></th> 
        <td class='desc' style="border-bottom: none;">
        <textarea rows="5" cols="100" id="wpa-mailingaddress" name="wpa-mailingaddress"><?php echo $mailingaddress; ?></textarea>
        <br />
        <p><?php _e('Provide your mailing address, so that auction winners can send you checks or money orders.','wpauctions') ?></p></td> 
      </tr> 
    </table>

    <p class="submit">
      <input type="submit" name="Submit" value="<?php _e('Update Options','wpauctions') ?> &raquo;" />
    </p>

  <h2 class="other-settings"><em><?php _e('Other Settings','wpauctions') ?></em></h2> 

    <table width="100%" cellspacing="2" cellpadding="5" class="widefat" style="margin-top: 1em;"> 

      <tr valign="top"> 
        <th scope="row" class='row-title'><?php _e('Extra Bidding Time:','wpauctions') ?></th> 
        <td class='desc'>
        <select id="wpa-endincrement" name="wpa-endincrement" onclick="CheckEndIncrementOptions()">
                <option value="1" <?php if ($endtrigger=='') echo 'selected'; ?>><?php _e('None, auction will end at specified time','wpauctions') ?></option>
                <option value="2" <?php if ($endtrigger!='') echo 'selected'; ?>><?php _e('Allow auction end time extension','wpauctions') ?></option>
         </select>
        <br />
        <div id="wpa_endincrementtab" style="display:<?php if ($endtrigger==''){ echo "none"; }?>;">
          <div><?php _e('Specify Time Window (number of minutes before closing when auction will be extended). Example, 30:','wpauctions') ?>
             <br /><input name="wpa-endtrigger" type="text" id="wpa-endtrigger" value="<?php echo $endtrigger; ?>"  /></div>
          <div><?php _e('Specify Time Extension (number of minutes auction will be extended by if a bid is placed in the End Time Bracket). Example, 15:','wpauctions') ?>
             <br /><input name="wpa-endoffset" type="text" id="wpa-endoffset" value="<?php echo $endoffset; ?>"  /></div>
        </div>
        <p><?php _e('If a bid is placed X minutes before closing, the auction will be extended by Y minutes (X=Time Window, Y=Time Extension).','wpauctions') ?></p>
  		</td>
      </tr> 

      <tr valign="top"> 
        <th scope="row" class='row-title'><?php _e('Show Countdown:','wpauctions') ?></th> 
        <td class='desc'>
        <select id="wpa-countdowndd" name="wpa-countdowndd" onclick="CheckCountdownOptions()">
                <option value="1" <?php if ($countdown=='') echo 'selected'; ?>><?php _e('No, just show end time','wpauctions') ?></option>
                <option value="2" <?php if ($countdown!='') echo 'selected'; ?>><?php _e('Show countdown as auction approached end','wpauctions') ?></option>
                <option value="3" <?php if ($countdown=='999') echo 'selected'; ?>><?php _e('Always show countdown','wpauctions') ?></option>
         </select>
        <br />
        <div id="wpa_countdowntab" style="display:<?php if (($countdown=='')||($countdown=='999')){ echo "none"; }?>;">
          <div><?php _e('Specify minutes before end (Example: 10):','wpauctions') ?><br /><input name="wpa-countdown" type="text" id="wpa-countdown" value="<?php echo $countdown; ?>" size="5" /></div>
        </div>
      </tr> 

      <tr valign="top"> 
        <th scope="row" class='row-title'><?php _e('Override Stye:','wpauctions') ?></th> 
        <td class='desc'>
        <select id="wpa-overridestyledd" name="wpa-overridestyledd" onclick="CheckOverrideStyle()">
                <option value="1" <?php if ($overridestyle=='') echo 'selected'; ?>><?php _e('No, use built-in style settings','wpauctions') ?></option>
                <option value="2" <?php if ($overridestyle!='') echo 'selected'; ?>><?php _e('I want to specify my own stylesheet','wpauctions') ?></option>
         </select>
        <br />
        <div id="wpa_overridestyletab" style="display:<?php if ($overridestyle==''){ echo "none"; }?>;">
          <div><?php _e('Specify URL to stylesheet:','wpauctions') ?><br /><input name="wpa-overridestyle" type="text" id="wpa-overridestyle" value="<?php echo $overridestyle; ?>" size="5" /></div>
        </div>
      </tr> 
    
      <tr valign="top"> 
        <th scope="row" class='row-title'><?php _e('List Format:','wpauctions') ?></th> 
        <td class='desc'>
        <select id="wpa-list" name="wpa-list">
                <option value="" <?php if ($list=='') echo 'selected'; ?>><?php _e('No, I prefer a graphical format','wpauctions') ?></option>
                <option value="Yes" <?php if ($list=='Yes') echo 'selected'; ?>><?php _e('Yes, show auctions in list format','wpauctions') ?></option>
         </select>
        <br />
        <p><?php _e('Select whether you prefer the sidebar widget to show a graphical or list format','wpauctions') ?></p></td> 
      </tr>
      <tr valign="top" class="alternate"> 
        <th scope="row" class='row-title'><?php _e('Auction Landing Page:','wpauctions') ?></th> 
        <td class='desc'><input name="wpa-landingpage" type="text" id="wpa-landingpage" value="<?php echo $landingpage; ?>" size="10" />
        <br />
        <p><?php _e('By default the plugin expects the auction widget to be on the front page. If this is not the case, or you have a dedicated Auctions page, you can specify an alternative landing page.','wpauctions') ?></p></td> 
      </tr> 
      <tr valign="top" class="alternate"> 
        <th scope="row" class='row-title'><?php _e('Custom Contact Field:','wpauctions') ?></th> 
        <td class='desc'><input name="wpa-customcontact" type="text" id="wpa-customcontact" value="<?php echo $customcontact; ?>" size="10" />
        <br />
        <p><?php _e('By default this is the "URL" field on the bid form. You can rename it to "Address" or "Phone" if you want to collect alternative information. Leave blank for default setting.','wpauctions') ?></p></td> 
      </tr> 
      <tr valign="top"> 
        <th scope="row" class='row-title'><?php _e('Plugin Love:','wpauctions') ?></th> 
        <td class='desc'>
        <select id="wpa-showattrib" name="wpa-showattrib">
                <option value="No" <?php if ($showattrib=='No') echo 'selected'; ?>><?php _e('Hide Attribution','wpauctions') ?></option>
                <option value="" <?php if ($showattrib=='') echo 'selected'; ?>><?php _e('Show Attribution','wpauctions') ?></option>
         </select>
        <br />
        <p><?php _e('You can enable a small link in your popup to www.wpauctions.com if you like.','wpauctions') ?></p></td> 
      </tr> 
      <tr valign="top" class="alternate"> 
        <th scope="row" class='row-title'><?php _e('RSS Feed link:','wpauctions') ?></th> 
        <td class='desc'>
        <select id="wpa-showrss" name="wpa-showrss">
                <option value="No" <?php if ($showrss=='No') echo 'selected'; ?>><?php _e('Hide RSS link','wpauctions') ?></option>
                <option value="" <?php if ($showrss=='') echo 'selected'; ?>><?php _e('Show RSS link','wpauctions') ?></option>
         </select>
        <br />
        <p><?php _e('Do you want to publish a link to your auction RSS feed. This can let people know when you publish new auctions','wpauctions') ?></p></td> 
      </tr> 
      <tr valign="top"> 
        <th scope="row" class='row-title'><?php _e('"No Auction" Alternative:','wpauctions') ?></th> 
        <td class='desc'>
        <textarea rows="5" cols="100" id="wpa-noauction" name="wpa-noauction"><?php echo $noauction; ?></textarea>
        <br />
        <p><?php _e('Specify the HTML you would like to display if there are no active auctions. Leave blank for standard "No Auctions" display<br>To rotate ads, separate with &lt;!--more--&gt;','wpauctions') ?></p></td> 
      </tr> 
    </table>

    <p class="submit">
      <input type="submit" name="Submit" value="<?php _e('Update Options','wpauctions') ?> &raquo;" />
    </p>

  <h2 class="other-settings"><em><?php _e('Issue Resolution Options','wpauctions') ?></em></h2> 

    <table width="100%" cellspacing="2" cellpadding="5" class="widefat" style="margin-top: 1em;"> 

      <tr valign="top" class="alternate"> 
        <th scope="row" class='row-title'><?php _e('Change Refresh Timeout:','wpauctions') ?></th> 
        <td class='desc'><input name="wpa-refreshtimeout" type="text" id="wpa-refreshtimeout" value="<?php echo $refreshtimeout; ?>" size="40" />
        <br />
        <p><?php _e('Enter the number of seconds to wait between auto-refreshes. Specify "none" if you want to disable this feature. (Default is 10 seconds). <b>Please note if you set this too low for your server, the refresh will timeout and wait 30 seconds before retrying.</b>','wpauctions') ?></p></td> 
      </tr> 


      <tr valign="top" class="alternate"> 
        <th scope="row" class='row-title'><?php _e('Bypass Resizer:','wpauctions') ?></th> 
        <td class='desc'>
        <select id="wpa-bypassresizer" name="wpa-bypassresizer">
                <option value="" <?php if ($bypassresizer=='') echo 'selected'; ?>><?php _e('Use Image Resizer','wpauctions') ?></option>
                <option value="Yes" <?php if ($bypassresizer=='Yes') echo 'selected'; ?>><?php _e('Avoid Using Resizer','wpauctions') ?></option>
          </select>
        <br />
        <p><?php _e('If the Image Resizer is giving you problems, you can bypass is here.','wpauctions') ?></p></td> 
      </tr> 

      <tr valign="top" class="alternate"> 
        <th scope="row" class='row-title'><?php _e('Bypass UTF8 Email:','wpauctions') ?></th> 
        <td class='desc'>
        <select id="wpa-bypassUTF8email" name="wpa-bypassUTF8email">
                <option value="" <?php if ($bypassUTF8email=='') echo 'selected'; ?>><?php _e('Use UTF8 Email','wpauctions') ?></option>
                <option value="Yes" <?php if ($bypassUTF8email=='Yes') echo 'selected'; ?>><?php _e('Avoid Using UTF8 Email','wpauctions') ?></option>
          </select>
        <br />
        <p><?php _e('If you are having problems with email, you can bypass Unicode email here. This is a known issue with GoDaddy hosting. (Please note you will not be able to use non-English charactersets)','wpauctions') ?></p></td> 
      </tr> 

      <tr valign="top" class="alternate"> 
        <th scope="row" class='row-title' style="border-bottom: 0;"><?php _e('Allow Remote Debug:','wpauctions') ?></th> 
        <td class='desc' style="border-bottom: 0;">
        <select id="wpa-remotedebug" name="wpa-remotedebug">
                <option value="" <?php if ($remotedebug=='') echo 'selected'; ?>><?php _e('Disallow Remote Debug','wpauctions') ?></option>
                <option value="Yes" <?php if ($remotedebug=='Yes') echo 'selected'; ?>><?php _e('Allow the WP Auctions support team access for Remote Debug','wpauctions') ?></option>
         </select>
        <br />
        <p><?php _e('If you experience any errors please activate this on our specific request ONLY. It will provide us your server information to assist remote debugging. Your information will be visible','wpauctions'); echo ' <a href="'.WPA_PLUGIN_FULL_PATH.'?debug">'; _e('here','wpauctions'); echo '</a>. '; _e('More information here - <a href="http://php.net/manual/en/function.phpinfo.php">PHP Config Information</a>','wpauctions') ?></p></td> 
      </tr> 

    </table>

	<input type="hidden" id="wp_auctions-submit" name="wp_auctions-submit" value="1" />

    <p class="submit">
      <input type="submit" name="Submit" value="<?php _e('Update Options','wpauctions') ?> &raquo;" />
    </p>
  </form> 
</div>

<?php
}


function wp_auctions_welcome() {

global $wpa_version;
global $wp_version;

// first let's check if database is update date
wp_auctions_install();

// Use WordPress built-in RSS handling
require_once (ABSPATH . WPINC . '/rss.php');
$rss_feed = "http://www.wpauctions.com/feed/";
$rss = @fetch_rss( $rss_feed );

?>
<link href="../wp-content/plugins/wp-auctions/requisites/style.css" rel="stylesheet" type="text/css" />

<div class="wrap wp-auctions">
	
	<div class="wpa-intro">
  
	<div class="wpa-info">
	  	<h3 class="wpa-about"><?php _e('Plugin Details','wpauctions'); ?></h3>
        	<p><strong><?php _e('Version:','wpauctions') ?></strong> <?php echo $wpa_version ?> Pro. <strong><?php _e('Database:','wpauctions') ?></strong> <?php echo get_option("wpa_db_version"); ?>. <strong><?php _e('Locale:','wpauctions') ?></strong> <?php echo get_locale(); ?>.</p>
            <!-- <p><strong><?php // _e('License:','wpauctions') ?></strong> <?php // _e('Multiple domain','wpauctions') ?>.</p> -->
            <p><strong><?php _e('License:','wpauctions') ?></strong> <?php _e('Single domain','wpauctions') ?>.</p>
      	<h3 class="wpa-resources"><?php _e('Resources','wpauctions') ?></h3>
      		<p><a href="http://www.wpauctions.com/faq/"><?php _e('F.A.Q','wpauctions') ?></a> / <a href="http://www.wpauctions.com/documentation/"><?php _e('Documentation','wpauctions'); ?></a> / <a href="http://www.weborithm.com/products/login.php"><?php _e('Upgrade for support','wpauctions') ?></a> (<?php _e('Get $49 off, use code 3840E','wpauctions') ?>)</p>
    </div>
    
	<div class="latestnews">
        <h3><?php _e('Plugin News','wpauctions') ?></h3>
        <ul>
        <?php
        if ( isset($rss->items) && 1 < count($rss->items) ) {
        $rss->items = array_slice($rss->items, 0, 5);
        foreach ($rss->items as $item ) {
        ?>
          <li><a href="<?php echo wp_filter_kses($item['link']); ?>"><?php echo wptexturize(esc_html($item['title'])); ?></a></li>
        <?php } ?>
        </ul>
        <?php
        }
        else {
          _e('No news found ..','wpauctions');
        }
        ?>
    </div>

    <div style="clear:both"></div>
</div>

<h2><?php _e('Get Started:','wpauctions'); ?></h2>

<ul class="wpa-start">
	<li><div class="buttons"><button onclick="window.location = 'admin.php?page=wp-auctions-add';" class="button"><strong><?php _e('New Auction','wpauctions'); ?></strong></button></div></li>
    <li><div class="buttons">/ &nbsp;<button onclick="window.location = 'admin.php?page=wp-auctions-manage';" class="button"><strong><?php _e('Current Auctions','wpauctions'); ?></strong></button></div></li>
</ul>
<div style="clear:both"></div>

<?php wp_auctions_options();  ?>

</div>

<?php   
}


function wpa_resetgetvars()
{
	unset($GLOBALS['_GET']["wpa_action"]);
	unset($GLOBALS['_GET']["wpa_id"]);
}

function wpa_chkfields($strName, $strDescription,$strEndDate)
{
	if($strName == "" || $strDescription == "" || $strEndDate == ""):
		$bitError = 1;
	endif;
	return $bitError;
}

function wpa_chkPrices($StartPrice, $ReservePrice,$BINPrice)
{
  if (($StartPrice < 0.01) && ($BINPrice <0.01)):
		$bitError = 1;
	elseif($ReservePrice > 0 && ($ReservePrice - $StartPrice) < 0):
		$bitError = 1;
	elseif($BINPrice > 0 && ($BINPrice - $StartPrice) < 0):
		$bitError = 1;
	endif;
	
	return $bitError;
}

function wp_auctions_add() {

   global $wpdb;
   $table_name = $wpdb->prefix . "wpa_auctions";
   $template_table_name = $wpdb->prefix . "wpa_auctiontemplates";

   $options = get_option('wp_auctions');
   $paypal = $options['paypal'];
   $mailingaddress = $options['mailingaddress'];
   $bankdetails = $options['bankdetails'];
   $engine = $options['engine'];
   
   // init all (to avoid warnings)
   $strSaveName = "";
   $strSaveDescription = "";
   $strSaveImageURL = "";
   $strSaveStartPrice = "";
   $strSaveReservePrice = "";
   $strSaveBINPrice = "";
   $strSaveDuration = "";
   $strStaticPage = "";
   $strSaveEndDate = "";
   $strSaveVariableShipping = "";
   $strSaveShippingPrice = "";
   $strSaveShippingFrom = "";
   $strSaveShippingTo = "";
   $strSaveImageURL1 = "";
   $strSaveImageURL2 = "";
   $strSaveImageURL3 = "";
   $strPaymentMethod = "";
   $strTerms = "";
   $strSaveStartDate = "";
   $strCustomIncrement = "";
  
   $intTemplateID = (isset($_POST["wpa_Template"]) ? $_POST["wpa_Template"] : "" );
     
   // Primary action
   if(isset($_REQUEST["wpa_action"])):

      // security check
      check_admin_referer( 'WPA-nonce');
      
      // handle a file uploads if there are any
		  $overrides = array('test_form' => false);
			
			// TODO: This needs to be refactored
			
			// handle each file individually - needs refactoring
		  $file = wp_handle_upload($_FILES['upload_0'], $overrides);

      if ( !isset($file['error']) ) {
         $url = $file['url'];
         $type = $file['type'];
         $file = $file['file'];
         $filename = basename($file);

         // Construct the object array
         $object = array(
           'post_title' => $filename,
           'post_content' => $url,
           'post_mime_type' => $type,
           'guid' => $url);

         // Save the data
         $id = wp_insert_attachment($object, $file);

         // Add the meta-data
         wp_update_attachment_metadata( $id, wp_generate_attachment_metadata( $id, $file ) );

         do_action('wp_create_file_in_uploads', $file, $id); // For replication
      
         $strSaveImageURL = $url;
                  
      } else {
         $strSaveImageURL = $_POST["wpa_ImageURL"];
      }

		  $file1 = wp_handle_upload($_FILES['upload_1'], $overrides);

      if ( !isset($file1['error']) ) {
         $url = $file1['url'];
         $type = $file1['type'];
         $file = $file1['file'];
         $filename = basename($file);

         // Construct the object array
         $object = array(
           'post_title' => $filename,
           'post_content' => $url,
           'post_mime_type' => $type,
           'guid' => $url);

         // Save the data
         $id = wp_insert_attachment($object, $file);

         // Add the meta-data
         wp_update_attachment_metadata( $id, wp_generate_attachment_metadata( $id, $file ) );

         do_action('wp_create_file_in_uploads', $file, $id); // For replication
      
         $strSaveImageURL1 = $url;
      } else {
         $strSaveImageURL1 = $_POST["wpa_ImageURL1"];
      }

		  $file2 = wp_handle_upload($_FILES['upload_2'], $overrides);

      if ( !isset($file2['error']) ) {
         $url = $file2['url'];
         $type = $file2['type'];
         $file = $file2['file'];
         $filename = basename($file);

         // Construct the object array
         $object = array(
           'post_title' => $filename,
           'post_content' => $url,
           'post_mime_type' => $type,
           'guid' => $url);

         // Save the data
         $id = wp_insert_attachment($object, $file);

         // Add the meta-data
         wp_update_attachment_metadata( $id, wp_generate_attachment_metadata( $id, $file ) );

         do_action('wp_create_file_in_uploads', $file, $id); // For replication
      
         $strSaveImageURL2 = $url;
      } else {
         $strSaveImageURL2 = $_POST["wpa_ImageURL2"];
      }

		  $file3 = wp_handle_upload($_FILES['upload_3'], $overrides);

      if ( !isset($file3['error']) ) {
         $url = $file3['url'];
         $type = $file3['type'];
         $file = $file3['file'];
         $filename = basename($file);

         // Construct the object array
         $object = array(
           'post_title' => $filename,
           'post_content' => $url,
           'post_mime_type' => $type,
           'guid' => $url);

         // Save the data
         $id = wp_insert_attachment($object, $file);

         // Add the meta-data
         wp_update_attachment_metadata( $id, wp_generate_attachment_metadata( $id, $file ) );

         do_action('wp_create_file_in_uploads', $file, $id); // For replication
      
         $strSaveImageURL3 = $url;
      } else {
         $strSaveImageURL3 = $_POST["wpa_ImageURL3"];
      }

      // process template fields
      if ($intTemplateID > 0) { 

          $strSQL = "SELECT * FROM $template_table_name WHERE id=".$intTemplateID;
          $row = $wpdb->get_row ($strSQL);
          $data = Array( 'form_id'=> $intTemplateID, 'form_structure' => $row->postdesc);

          $form = new Formbuilder($data);
          $templatefields = $form->process();
      }


      if($_POST["wpa_action"] == "Add Auction"):
         $strSaveName = strip_tags(htmlspecialchars($_POST["wpa_name"]));
         $strSaveDescription = $_POST["wpa_description"];
         $strSaveStartPrice = $_POST["wpa_StartPrice"];
         $strSaveReservePrice = $_POST["wpa_ReservePrice"];
         $strSaveBINPrice = $_POST["wpa_BINPrice"];
         $strSaveEndDate = $_POST["wpa_EndDate"];
         $strSaveStartDate = $_POST["wpa_StartDate"];
         $strSaveVariableShipping = $_POST["wpa_VariableShipping"];
         $strSaveShippingPrice = $_POST["wpa_ShippingPrice"];
         $strSaveShippingTo = strip_tags(htmlspecialchars($_POST["wpa_ShippingTo"]));
         $strSaveShippingFrom = strip_tags(htmlspecialchars($_POST["wpa_ShippingFrom"]));                           
         $strStaticPage = $_POST["wpa_StaticPage"];     
         $strPaymentMethod = $_POST["wpa_PaymentMethod"];              
         $strTerms = $_POST["wpa_Terms"]; 
         $strCustomIncrement = $_POST["wpa_CustomIncrement"]; 
      elseif($_POST["wpa_action"] == "Update Auction"):
         $strUpdateID = $_POST["wpa_id"];
         $strSaveName = strip_tags(htmlspecialchars($_POST["wpa_name"]));
         $strSaveDescription = $_POST["wpa_description"];
         $strSaveStartPrice = $_POST["wpa_StartPrice"];
         $strSaveReservePrice = $_POST["wpa_ReservePrice"];
         $strSaveBINPrice = $_POST["wpa_BINPrice"];
         $strSaveEndDate = $_POST["wpa_EndDate"];
         $strSaveStartDate = $_POST["wpa_StartDate"];
         $strSaveVariableShipping = $_POST["wpa_VariableShipping"];
         $strSaveShippingPrice = $_POST["wpa_ShippingPrice"];
         $strSaveShippingTo = strip_tags(htmlspecialchars($_POST["wpa_ShippingTo"]));
         $strSaveShippingFrom = strip_tags(htmlspecialchars($_POST["wpa_ShippingFrom"]));
         $strStaticPage = $_POST["wpa_StaticPage"];
         $strPaymentMethod = $_POST["wpa_PaymentMethod"];              
         $strTerms = $_POST["wpa_Terms"];  
         $strCustomIncrement = $_POST["wpa_CustomIncrement"];  
         $bolUpdate = true;
      elseif($_GET["wpa_action"] == "edit"):
         $strSQL = "SELECT * FROM ".$table_name." WHERE id=".$_GET["wpa_id"];
         $resultEdit = $wpdb->get_row($strSQL);
         $strUpdateID = $_GET["wpa_id"];
         $strSaveName = htmlspecialchars_decode($resultEdit->name, ENT_NOQUOTES);
         $strSaveDescription = stripslashes($resultEdit->description);
         $strSaveImageURL = $resultEdit->image_url;
         $strSaveStartPrice = $resultEdit->start_price;
         $strSaveReservePrice = $resultEdit->reserve_price;
         $strSaveBINPrice = $resultEdit->BIN_price;
         $strSaveEndDate = get_date_from_gmt($resultEdit->date_end);
         $strSaveStartDate = get_date_from_gmt($resultEdit->date_start);
         $strSaveVariableShipping = $resultEdit->variable_shipping;
         $strSaveShippingPrice = $resultEdit->shipping_price;
         $strSaveShippingFrom = htmlspecialchars_decode($resultEdit->shipping_from, ENT_NOQUOTES);
         $strSaveShippingTo = htmlspecialchars_decode($resultEdit->shipping_to, ENT_NOQUOTES);                  
         $strSaveImageURL1 = $resultEdit->extraimage1;
         $strSaveImageURL2 = $resultEdit->extraimage2;
         $strSaveImageURL3 = $resultEdit->extraimage3;
         $strStaticPage = $resultEdit->staticpage;
         $strPaymentMethod = $resultEdit->paymentmethod;
         $strTerms = $resultEdit->terms;
         $strPostDesc = $resultEdit->postdesc;
         $strCustomIncrement = $resultEdit->customincrement;
                  
         $template = unserialize($strPostDesc);
         $intTemplateID2 = $template["template"];
         $strTemplateData = $template["data"];

         $bolUpdate = true;
         wpa_resetgetvars();
      elseif($_GET["wpa_action"] == "relist"):
         $strSQL = "SELECT * FROM ".$table_name." WHERE id=".$_GET["wpa_id"];
         $resultList = $wpdb->get_row($strSQL);
         $strSaveName = htmlspecialchars_decode($resultList->name, ENT_NOQUOTES);
         $strSaveDescription = stripslashes($resultList->description);
         $strSaveImageURL = $resultList->image_url;
         $strSaveStartPrice = $resultList->start_price;
         $strSaveReservePrice = $resultList->reserve_price;
         $strSaveBINPrice = $resultList->BIN_price;
         $strSaveEndDate = get_date_from_gmt($resultList->date_end);
         $strSaveStartDate = get_date_from_gmt($resultList->date_start);
         $strSaveVariableShipping = $resultEdit->variable_shipping;
         $strSaveShippingPrice = $resultEdit->shipping_price;
         $strSaveShippingFrom = htmlspecialchars_decode($resultEdit->shipping_from, ENT_NOQUOTES);
         $strSaveShippingTo = htmlspecialchars_decode($resultEdit->shipping_to, ENT_NOQUOTES);                  
         $strSaveImageURL1 = $resultList->extraimage1;
         $strSaveImageURL2 = $resultList->extraimage2;
         $strSaveImageURL3 = $resultList->extraimage3;
         $strStaticPage = $resultList->staticpage;
         $strPaymentMethod = $resultList->paymentmethod;
         $strTerms = $resultList->terms;
         $strCustomIncrement = $resultList->customincrement;

         $strPostDesc = $resultList->postdesc;
         
         $template = unserialize($strPostDesc);
         $intTemplateID2 = $template["template"];
         $strTemplateData = $template["data"];

         wpa_resetgetvars();
      endif;
   endif;

   // make sure drop down takes precedence over database value
   if ( $intTemplateID == 0 ) { $intTemplateID = $intTemplateID2; }

   // Validation & Save
   if($_POST["wpa_action"] == "Add Auction"):
      if(wpa_chkfields($strSaveName, $strSaveDescription,$strSaveEndDate)==1):
         $strMessage = __('Please fill out all fields.','wpauctions');
      elseif(strtotime($strSaveEndDate) < strtotime(get_date_from_gmt(date('Y-m-d H:i:s')))):      
         $strMessage = __('Auction end date/time cannot be in the past','wpauctions').": (Specified: ".$strSaveEndDate." - Current: ".get_date_from_gmt(date('Y-m-d H:i:s')).")";
      elseif(wpa_chkPrices($strSaveStartPrice,$strSaveReservePrice,$strSaveBINPrice) == 1):
         $strMessage = __('Starting Price must be numeric and less than Reserve and BIN Prices','wpauctions');
      elseif (isset($templatefields) || $templatefields["success"] == false):
         if (is_array($templatefields["errors"])) {
            foreach ( $templatefields["errors"] as $error) {
              $strMessage .= $error . "<br>";
            }
          }
      endif;

      if ($strMessage == ""):
         // force reserve value ,BINPrice and Shipping Price to ensure value written in InnoDB (which doesn't like Null decimals)
         $strSaveReservePrice = $strSaveReservePrice + 0;
         $strSaveDuration = 0;  // depracated
         $strSaveBINPrice = $strSaveBINPrice + 0;
         $strSaveShippingPrice = $strSaveShippingPrice + 0;

         // convert date/time to GMT
         $strSaveEndDate = get_gmt_from_date($strSaveEndDate);
         $GMTTime = current_time('mysql',"1");

         if ($strSaveStartDate == "") {
            $strSaveStartDate = $GMTTime;
         } else {
            $strSaveStartDate = get_gmt_from_date($strSaveStartDate);
         }

         // prepare template fields for database
         $postdesc = serialize( array ( "template" => $intTemplateID, "data" => $templatefields["results"] ) );
         $strSQL = "INSERT INTO $table_name (date_create,date_end,date_end_original,name,description,image_url,start_price,reserve_price,BIN_price,duration,variable_shipping,shipping_price,shipping_from,shipping_to,extraimage1,extraimage2,extraimage3,staticpage,paymentmethod,terms,engine,date_start,postdesc,customincrement) VALUES('".$GMTTime."','".$strSaveEndDate."','".$strSaveEndDate."','".$strSaveName."','".$strSaveDescription."','".$strSaveImageURL."','".$strSaveStartPrice."','".$strSaveReservePrice."','".$strSaveBINPrice."','".$strSaveDuration."','".$strSaveVariableShipping."','".$strSaveShippingPrice."','".$strSaveShippingFrom."','".$strSaveShippingTo."','".$strSaveImageURL1."','".$strSaveImageURL2."','".$strSaveImageURL3."','".$strStaticPage."','".$strPaymentMethod."','".$strTerms."','".$engine."','".$strSaveStartDate."','".$postdesc."','".$strCustomIncrement."')";
         
         // defensive check to make sure noone's put "|" in any field (as this breaks AJAX)
         $strSQL = str_replace( "|" , "" , $strSQL );
         
         if ($wpdb->query($strSQL) == false) {
            $strMessage = __('Error adding auction','wpauctions');
            $wpdb->print_error();
         } else {
            $strMessage = __('Auction added','wpauctions');
         }
         
         $strSaveName = "";
         $strSaveDescription = "";
         $strSaveImageURL = "";
         $strSaveStartPrice = "";
         $strSaveReservePrice = "";
         $strSaveBINPrice = "";
         $strSaveDuration = "";
         $strStaticPage = "";
         $strSaveEndDate = "";
         $strSaveVariableShipping = "";
         $strSaveShippingPrice = "";
         $strSaveShippingFrom = "";
         $strSaveShippingTo = "";
         $strSaveImageURL1 = "";
         $strSaveImageURL2 = "";
         $strSaveImageURL3 = "";
         $strPaymentMethod = "";
         $strTerms = "";
         $strSaveStartDate = "";
         $strCustomIncrement = "";
      endif;
      wpa_resetgetvars();
   elseif($_POST["wpa_action"] == "Update Auction"):
      if(wpa_chkfields($strSaveName, $strSaveDescription,$strSaveStartPrice,$strSaveDuration)==1):
         $strMessage = __('Please fill out all fields.','wpauctions');
      elseif(strtotime($strSaveEndDate) < strtotime(get_date_from_gmt(date('Y-m-d H:i:s')))):      
         $strMessage = __('Auction end date/time cannot be in the past','wpauctions').": (Specified: ".$strSaveEndDate." - Current: ".get_date_from_gmt(date('Y-m-d H:i:s')).")";
      elseif(wpa_chkPrices($strSaveStartPrice,$strSaveReservePrice,$strSaveBINPrice) == 1):
         $strMessage = __('Starting Price must be numeric and less than Reserve and BIN Prices','wpauctions');
      elseif (isset($templatefields) || $templatefields["success"] == false):
         if (is_array($templatefields["errors"])) {
            foreach ( $templatefields["errors"] as $error) {
              $strMessage .= $error . "<br>";
           }
         }
      endif;

      if ($strMessage == ""):
         // force reserve value,BINPrice and Shipping Price to ensure value written in InnoDB (which doesn't like Null decimals)
         $strSaveReservePrice = $strSaveReservePrice + 0;
         $strSaveDuration = 0;  // depracated
         $strSaveBINPrice = $strSaveBINPrice + 0;
         $strSaveShippingPrice = $strSaveShippingPrice + 0;

         // convert date/time to machine - using diff variables not to upset display!
         $strSaveEndDateGMT = get_gmt_from_date($strSaveEndDate);
         $strSaveStartDateGMT = get_gmt_from_date($strSaveStartDate);

         $postdesc = serialize( array ( "template" => $intTemplateID, "data" => $templatefields["results"] ) );

         // prepare template fields for database
         $strSQL = "UPDATE $table_name SET name='$strSaveName', description = '$strSaveDescription', image_url = '$strSaveImageURL', start_price = '$strSaveStartPrice', reserve_price = '$strSaveReservePrice', BIN_price = '$strSaveBINPrice', duration = '$strSaveDuration', shipping_price = '$strSaveShippingPrice', variable_shipping = '$strSaveVariableShipping', shipping_from = '$strSaveShippingFrom', shipping_to = '$strSaveShippingTo', date_end = '$strSaveEndDateGMT', extraimage1 = '$strSaveImageURL1', extraimage2 = '$strSaveImageURL2', extraimage3 = '$strSaveImageURL3', staticpage = '$strStaticPage', paymentmethod = '$strPaymentMethod', terms = '$strTerms', date_start='$strSaveStartDateGMT', postdesc = '$postdesc', customincrement = '$strCustomIncrement' WHERE id=" . $_POST["wpa_id"];

         // defensive check to make sure noone's put "|" in any field (as this breaks AJAX)
         $strSQL = str_replace( "|" , "" , $strSQL );

         $strMessage = "Auction updated";
         //$bolUpdate = false;
         
         $wpdb->query($strSQL);
         wpa_resetgetvars();
      endif;
   endif;
		
	// clean up slashes for editor		
	$strSaveDescription = stripslashes($strSaveDescription);		
	$strSaveName = stripslashes($strSaveName);	
   ?>
   
   <link href="../wp-content/plugins/wp-auctions/requisites/style.css" rel="stylesheet" type="text/css" />
   
	<div class="wrap wp-auctions">
		
		<?php if($strMessage != ""):?>
			<fieldset class="options">
				<!-- <legend><?php // _e('Information','wpauctions'); ?></legend> -->
				<div class="wpa-check"><?php print $strMessage ?></div>
			</fieldset>
		<?php endif; ?>

  
   <?php 
   if ($engine < 1) { 
     echo "<h2>Main settings not defined</h2>Please check main settings and try again. This can happen if you've just upgraded the plugin and haven't set an Auction Engine on the settings page (you need to make sure you have no active auctions to set this)"; 
   } else { ?>
   
    <div class="wpa-auction-engine"><?php _e('Auction mode','wpauctions'); ?>: <?php echo report_engine(); ?></div>
		<div class="wpa-template-to-use"><p><?php _e('Template to use','wpauctions'); ?>:</p>
		<form method="post" action="<?php admin_url('admin.php?page=page=wp-auctions-add'); ?>" id="templateselector">
        <select id="wpa_Template" name="wpa_Template" onchange="this.form.submit()">
          <option value=""><?php _e('Default (no template)','wpauctions'); ?></option>
        <?php 
        $strSQL = "SELECT * FROM $template_table_name";
        $rows = $wpdb->get_results ($strSQL);

        if (is_array($rows)): 
          foreach ($rows as $row) { 
        
              echo "<option value='".$row->id."'";
              if ($row->id == $intTemplateID) echo " SELECTED";
              echo ">".$row->templatename."</option>";
           }
         endif;
        ?>
        </select>		
    </form>
		</div>
		
	  <?php if($bolUpdate == true): ?>		

<?php
  $bid_table_name = $wpdb->prefix . "wpa_bids";
  $bid_count = $wpdb->get_var( $wpdb->prepare( "SELECT COUNT(*) FROM $bid_table_name WHERE auction_id=$strUpdateID;" ) );
?>
		  <h2 class="details"><em><?php _e('Update Auction','wpauctions'); ?> (<?php echo $bid_count." "; _e('bids','wpauctions'); ?>)</em></h2>
	  <?php else: ?>		
		  <h2 class="details"><em><?php _e('New Auction','wpauctions'); ?></em></h2>
		  <?php $bid_count = 0; ?>		  
	  <?php endif; ?>		

		<script language="Javascript">
		
		function showhide(){
		   var dropdown = jQuery("#popup").val();   
		   
		   if (dropdown == "No") {
			  jQuery("#optional_static_page").hide();
			  jQuery("input#wpa_StaticPage").val('');
		   } else {
			  jQuery("#optional_static_page").show();
		   }      
		}

		function showhideShipping(){
		   var dropdown = jQuery("#popupShipping").val();   
		   
		   if (dropdown == "No") {
			  jQuery("#optional_fixed_shipping").hide();
			  jQuery("#optional_variable_shipping").show();
			  jQuery("input#wpa_ShippingPrice").val('0');
		   } else {
			  jQuery("#optional_fixed_shipping").show();
			  jQuery("#optional_variable_shipping").hide();
			  jQuery("input#wpa_VariableShipping").val('');
		   }      
		}
		
		// show/hide optional element
		jQuery(document).ready(function() {
		  showhide();
		  showhideShipping();
		  
		  // set up datepicker
		  jQuery("#wpa_EndDate").datetimepicker({ dateFormat: 'yy-mm-dd', timeFormat: 'hh:mm:ss' });        
		  jQuery("#wpa_StartDate").datetimepicker({ dateFormat: 'yy-mm-dd', timeFormat: 'hh:mm:ss' });        
		  
		});
		
		</script>

		<form method="post" action="<?php admin_url('admin.php?page=page=wp-auctions-add'); ?>" id="editform" enctype="multipart/form-data">

    <?php wp_nonce_field('WPA-nonce'); ?>
    
    <table width="100%" cellspacing="2" cellpadding="5" class="widefat">
      <tr valign="top"> 
        <th scope="row"><?php _e('Title:','wpauctions') ?></th> 
        <td><input type="text" name="wpa_name" value="<?php print $strSaveName ?>" maxlength="255" size="50" /><br>
        <?php _e('Specify the title for your auction.','wpauctions') ?></td> 
      </tr>

      <tr valign="top" class="alternate"> 
        <th scope="row"><?php _e('Description:','wpauctions') ?></th> 
        <td>

        <?php
        $content = $strSaveDescription;
        $id = 'wpa_description';
        $settings = array(
                'quicktags' => array(
                        'buttons' => 'em,strong,link',
                ),
                'quicktags' => true,
                'tinymce' => true
        );
 
        wp_editor($content, $id, $settings);
        ?>        
        
        <br>
        <p><?php _e('Enter the description for your auction. Image uploads are handled below.','wpauctions') ?></p>
		  </td> 
      </tr>


      <?php 
      // show template fields if needed
      if ($intTemplateID > 0) { ?>

      <tr valign="top" class="alternate"> 
        <th scope="row"><?php _e('Template Fields:','wpauctions') ?></th> 
        <td>

        <?php
          // fetch template structure
          $strSQL = "SELECT * FROM $template_table_name WHERE id=".$intTemplateID;
          $row = $wpdb->get_row ($strSQL);
          $data = Array( 'form_id'=> $intTemplateID, 'form_structure' => $row->postdesc);            

          $form = new Formbuilder($data);
          $form->load_data($strTemplateData);
          echo $form->wpa_generate_html();
        ?>

        <p><?php _e('Enter the template fields for this item.','wpauctions') ?></p>
		  </td> 
      </tr>
        
      <?php
      }
      ?>

      <tr valign="top"> 
        <th scope="row"><?php _e('Image URL:','wpauctions') ?></th> 
        <td>
        <div class="preview0" style="float:right;">
           <img src="<?php echo wpa_resize ( $strSaveImageURL, 125 ) ?>" width="125px" height="125px" id="thumb_0">
        </div>

        <input type="text" name="wpa_ImageURL" value="<?php print $strSaveImageURL ?>" maxlength="255" size="50" id="upload_image_0"/>
        <p><?php _e('You can specify a URL to an image, alternatively upload one from your computer','wpauctions'); ?></p>
        <label for="upload_0"><?php _e('Choose an image from your computer:','wpauctions'); ?></label><br /><input type="file" id="upload_0" name="upload_0" /><br /><?php _e('If your images do not appear please CHMOD the "wp-auctions/files" folder 777 via FTP. <a href="http://codex.wordpress.org/Changing_File_Permissions#Using_an_FTP_Client" target="_blank">Instructions</a>.','wpauctions') ?>
        </td> 
      </tr>
      <tr valign="top" class="alternate"> 
        <th scope="row"><?php _e('Start Price:','wpauctions') ?></th> 
        <td><input type="text" name="wpa_StartPrice" value="<?php print $strSaveStartPrice ?>" maxlength="255" size="10" /><br>
        <?php _e('Specify the starting price for your auction. Leave empty (or 0) for Fixed Price BIN','wpauctions') ?>
        </td> 
      </tr>
            
      <tr valign="top" class="alternate"> 
        <th scope="row"><?php _e('End Date:','wpauctions') ?></th> 
        <td><input type="text" name="wpa_EndDate" id="wpa_EndDate" value="<?php print $strSaveEndDate ?>" maxlength="20" size="20" /><br>
        <?php _e('When would you like this auction to end?','wpauctions') ?> <strong><?php _e('Your current blog time is','wpauctions'); echo ": ".get_date_from_gmt(date('Y-m-d H:i:s')); ?></strong><br />
		<em><?php _e('The end time will advance to the current time if you click on a date in the calendar popup. You can still edit it manually back again.','wpauctions') ?></em>
</td> 
      </tr>
      <tr valign="top"> 
        <th scope="row"><?php _e('Auction Category:','wpauctions') ?></th> 
        <td>
        <select id="wpa_Terms" name="wpa_Terms">
          <option value="">Uncategorised</option>
        <?php 
           $myterms = get_terms('auction_category', 'get=all');
           foreach($myterms as $term) {
              echo "<option value='".$term->name."'";
              if ($term->name == $strTerms) echo " SELECTED";
              echo ">".$term->name."</option>";
           }
        ?>
        </select><br />
                
        <?php _e('Specify a category for your auction. You can <a href="edit-tags.php?taxonomy=auction_category">add new categories here</a>.','wpauctions') ?></td> 
      </tr>
      <tr valign="top" class="alternate"> 
        <th scope="row"><?php _e('Payment Method:','wpauctions') ?></th> 
        <td>
           <input name="wpa_PaymentMethod" id="wpa-radio" type="radio" value="paypal" <?php if ($strPaymentMethod=="paypal") echo "CHECKED";?> <?php if ($paypal=="") echo "DISABLED";?>>PayPal<br>
           <input name="wpa_PaymentMethod" id="wpa-radio" type="radio" value="bankdetails" <?php if ($strPaymentMethod=="bankdetails") echo "CHECKED";?> <?php if ($bankdetails=="") echo "DISABLED";?>><?php _e('Bank/Wire Transfer','wpauctions') ?><br>        
           <input name="wpa_PaymentMethod" id="wpa-radio" type="radio" value="mailingaddress" <?php if ($strPaymentMethod=="mailingaddress") echo "CHECKED";?> <?php if ($mailingaddress=="") echo "DISABLED";?>><?php _e('Cheque or Money Order','wpauctions') ?><br>        
        <?php _e('Specify the payment method for this auction (Only options you filled on the main configuration screen are available)','wpauctions') ?></td> 
      </tr>
      <tr valign="top"> 
        <th scope="row" style="border-bottom: 0;">
        <?php _e('Show auction in AJAX Popup?:','wpauctions') ?></th> 
        <td style="border-bottom: 0;">        
         <select id="popup" name="popup" onchange="showhide()">
                <option value="No" <?php if ($strStaticPage=='') echo 'selected'; ?>><?php _e('Yes','wpauctions') ?></option>
                <option value="Yes" <?php if ($strStaticPage!='') echo 'selected'; ?>><?php _e('No, show auction in a post','wpauctions') ?></option>
         </select>
        <br>
        <?php _e('If you do not want to use the popup, you can direct the auction to a <a href="edit.php">Post</a> or <a href="edit.php?post_type=page">Page</a> (you\'ll need to add the Auction shortcode to the page)','wpauctions') ?></td> 
      </tr>
      <tr valign="top" class="alternate" id="optional_static_page"> 
        <th scope="row" style="border-bottom: 0;">
        <?php _e('URL for Static Post/Page:','wpauctions') ?> </th> 
        <td style="border-bottom: 0;"><input type="text" id="wpa_StaticPage" name="wpa_StaticPage" value="<?php print $strStaticPage ?>" maxlength="255" size="50" /><br>
        <?php _e('Please specify the Post or Page URL where this auction will be inserted (you will need to insert the auction on the Post or Page manually).','wpauctions') ?></td> 
      </tr>
     </table>


   <h2 class="shipping"><em><?php _e('Shipping','wpauctions') ?></em></h2>
    <table width="100%" cellspacing="2" cellpadding="5" class="widefat"> 
      <tr valign="top"> 
        <th scope="row" style="border-bottom: 0;">
        <?php _e('Shipping Options:','wpauctions') ?></th> 
        <td style="border-bottom: 0;">        
         <select id="popupShipping" name="popupShipping" onchange="showhideShipping()">
                <option value="Yes" <?php if ($strSaveVariableShipping=='') echo 'selected'; ?>><?php _e('Shipping Price','wpauctions') ?></option>
                <option value="No" <?php if ($strSaveVariableShipping!='') echo 'selected'; ?>><?php _e('Shipping Description','wpauctions') ?></option>
         </select>
        <br>
        <?php _e('Specify a fixed shipping price or a description of how shipping is calculated','wpauctions') ?></td> 
      </tr>
      <tr valign="top" class="alternate" id="optional_variable_shipping"> 
        <th scope="row"><?php _e('Shipping Description:','wpauctions') ?></th> 
        <td style="border-top: 0;"><input type="text" id="wpa_VariableShipping" name="wpa_VariableShipping" value="<?php print $strSaveVariableShipping; ?>" maxlength="255" size="30" /><br>
        <?php _e('Enter your shipping description (example: Depends on destination).','wpauctions') ?></td> 
      </tr>
      <tr valign="top" class="alternate" id="optional_fixed_shipping"> 
        <th scope="row"><?php _e('Shipping Price:','wpauctions') ?></th> 
        <td style="border-top: 0;"><input type="text" id="wpa_ShippingPrice" name="wpa_ShippingPrice" value="<?php print $strSaveShippingPrice ?>" maxlength="255" size="10" /><br>
        <?php _e('Enter your shipping price.','wpauctions') ?></td> 
      </tr>
      <tr valign="top"> 
        <th scope="row"><?php _e('Shipping To:','wpauctions') ?></th> 
        <td><input type="text" name="wpa_ShippingTo" value="<?php print $strSaveShippingTo ?>" maxlength="255" size="50" /><br>
        <?php _e('Enter "Worldwide" or any location you can ship too.','wpauctions') ?></td> 
      </tr>
      <tr valign="top" class="alternate"> 
        <th scope="row" style="border-bottom: 0;"><?php _e('Shipping From:','wpauctions') ?></th> 
        <td style="border-bottom: 0;"><input type="text" name="wpa_ShippingFrom" value="<?php print $strSaveShippingFrom ?>" maxlength="255" size="50" /><br>
        <?php _e('Where are you shipping this item from?','wpauctions') ?></td> 
      </tr>
   </table>
   
   <h2 class="other-settings"><em><?php _e('Optional Settings','wpauctions') ?></em></h2>
    <table width="100%" cellspacing="2" cellpadding="5" class="widefat"> 
       <tr valign="top" class="alternate"> 
        <th scope="row"><?php _e('Buy It Now Price:','wpauctions') ?></th> 
        <td><input type="text" name="wpa_BINPrice" value="<?php print $strSaveBINPrice ?>" maxlength="255" size="10" />
        <p><?php _e('Specify the "Buy It Now" price for your auction.','wpauctions') ?></p></td> 
      </tr>

      <?php if ($engine <> 3) { // with standard and simple engines we can have a reserve price ?>
      <tr valign="top"> 
        <th scope="row"><?php _e('Reserve Price:','wpauctions') ?></th> 
        <td><input type="text" name="wpa_ReservePrice" value="<?php print $strSaveReservePrice ?>" maxlength="255" size="10" /><br>
        <?php _e('Specify the reserve price for your auction. Leave empty (or 0) if there is no reserve on this auction.','wpauctions') ?>
        </td> 
      </tr>
      <?php } ?>

	  <?php if($bid_count == 0): ?>		
      <tr valign="top" class="alternate"> 
        <th scope="row"><?php _e('Start Date:','wpauctions') ?></th> 
        <td><input type="text" name="wpa_StartDate" id="wpa_StartDate" value="<?php print $strSaveStartDate ?>" maxlength="20" size="20" /><br>
        <?php _e('When would you like this auction to start?','wpauctions') ?> <strong><?php _e('Your current blog time is','wpauctions'); echo ": ".get_date_from_gmt(date('Y-m-d H:i:s')); ?></strong><br />
		    <?php _e('(Leave blank to start immediately) N.B. This cannot be changed once there are bids on the auction.','wpauctions') ?>
		    </td> 
      </tr>
    <?php else: ?>
      <tr valign="top" class="alternate"> 
        <th scope="row"><?php _e('Start Date:','wpauctions') ?></th> 
        <td><strong><?php print $strSaveStartDate ?></strong><br>
		    <?php _e('N.B. This cannot be changed once there are bids on the auction.','wpauctions') ?>
		    </td> 
      </tr>
    <?php endif; ?>
 
 
      <tr valign="top" class="alternate"> 
        <th scope="row"><?php _e('Custom Increment:','wpauctions') ?></th> 
        <td><input type="text" name="wpa_CustomIncrement" value="<?php print $strCustomIncrement ?>" maxlength="255" size="10" />
        <p><?php _e('Specify the custom bidding increment for your auction. This will override any previously configured increment','wpauctions') ?></p></td> 
      </tr>

      <tr valign="top"> 
        <th scope="row"><?php _e('Extra Image URL 1:','wpauctions') ?></th> 
        <td>
        <div id="preview1" style="float:right;">
           <img src="<?php echo wpa_resize ( $strSaveImageURL1, 125 ) ?>" width="125" height="125">
        </div>
        <input type="text" name="wpa_ImageURL1" value="<?php print $strSaveImageURL1 ?>" maxlength="255" size="50" id="upload_image_1"/>
        <p><?php _e('You can specify a URL to an image, alternatively upload one from your computer','wpauctions'); ?></p>
        <label for="upload_1"><?php _e('Choose an image from your computer:','wpauctions'); ?></label><br /><input type="file" id="upload_1" name="upload_1" />
        </td>
      </tr>
      <tr valign="top" class="alternate"> 
        <th scope="row"><?php _e('Extra Image URL 2:','wpauctions') ?></th> 
        <td>
        <div id="preview2" style="float:right;">
           <img src="<?php echo wpa_resize ( $strSaveImageURL2, 125 ) ?>" width="125" height="125">
        </div>
        <input type="text" name="wpa_ImageURL2" value="<?php print $strSaveImageURL2 ?>" maxlength="255" size="50" id="upload_image_2"/>
        <p><?php _e('You can specify a URL to an image, alternatively upload one from your computer','wpauctions'); ?></p>
        <label for="upload_0"><?php _e('Choose an image from your computer:','wpauctions'); ?></label><br /><input type="file" id="upload_2" name="upload_2" />
        </td> 
      </tr>
      <tr valign="top"> 
        <th scope="row" style="border-bottom: 0;"><?php _e('Extra Image URL 3:','wpauctions') ?></th> 
        <td  style="border-bottom: 0;">
        <div id="preview3" style="float:right;">
           <img src="<?php echo wpa_resize ( $strSaveImageURL3, 125 ) ?>" width="125" height="125">
        </div>
        <input type="text" name="wpa_ImageURL3" value="<?php print $strSaveImageURL3 ?>" maxlength="255" size="50" id="upload_image_3"/>
        <p><?php _e('You can specify a URL to an image, alternatively upload one from your computer','wpauctions'); ?></p>
        <label for="upload_3"><?php _e('Choose an image from your computer:','wpauctions'); ?></label><br /><input type="file" id="upload_3" name="upload_3" />
        </td> 
      </tr>
   	</table>
		
		<input type="hidden" name="wpa_Template" id="wpa_Template" value="<?php echo $intTemplateID ?>">
		
	<?php if($bolUpdate == true): ?>
		<div class="buttons add-auction"><input type="hidden" name="wpa_id" value="<?php echo $strUpdateID ?>"><input type="hidden" name="wpa_action" value="Update Auction">
		<input type="submit" name="wpa_doit" value="<?php _e('Update Auction','wpauctions') ?>" class="button"></div>
	<?php else: ?>
		<div class="buttons add-auction"><input type="hidden" name="wpa_action" value="Add Auction"><input type="submit" name="wpa_doit" value="<?php _e('Add Auction','wpauctions') ?> &raquo;" class="button" ></div>
	<?php endif; ?>

	</form>
		<?php } // checking main setings ?>
		
	</div>
<?php
}


function wp_auctions_manage() {

   global $wpdb;

   $intAlternate = 0;
   // Primary action
   if(isset($_REQUEST["wpa_action"])) {

      // security check
      check_admin_referer( 'WPA-nonce');

      if($_GET["wpa_action"] == "reverse") {
         $intAuctionID = $_GET["wpa_id"];
         $intBidID = $_GET["bid_id"];
 
         // check that inputs are numeric         
         if (($intAuctionID > 0) && ($intBidID > 0)) {
           // get ready to reverse the last bid on the auction
           $bid_table_name = $wpdb->prefix . "wpa_bids";
           $auction_table_name = $wpdb->prefix . "wpa_auctions";

           // Step 1 - Delete Last bid
           $strSQL = "DELETE FROM $bid_table_name WHERE id=" . $intBidID;
           $wpdb->query($strSQL);

           // flush cache .. otherwise we'll just pick up an empty record on the next pass
           $wpdb->flush();

           // Step 2 - Assess highest bid
           $strSQL = "SELECT * FROM $bid_table_name WHERE auction_id=".$intAuctionID." ORDER BY current_bid_price DESC LIMIT 1";
           $current = $wpdb->get_row ($strSQL);

           // Step 3 - Update Auction with current bid price
           $sql = "UPDATE ".$auction_table_name." SET current_price = ".floatval($current->current_bid_price)." WHERE id=".$intAuctionID;
           $wpdb->query($sql);
           
         }         
      }
      elseif ($_GET["wpa_action"] == "terminate") {
         $intAuctionID = $_GET["wpa_id"];

         // check that inputs are numeric         
         if (($intAuctionID > 0)) {
           // get ready to reverse the last bid on the auction
           $auction_table_name = $wpdb->prefix . "wpa_auctions";

           // Step 1 - Update auction to set end timestamp to now
          $sql = "UPDATE ".$auction_table_name." SET date_end = '".current_time('mysql',"1")."' WHERE id=".$intAuctionID;
          $wpdb->query($sql);

           // wait a bit, to make sure Now() in termination check doesn't match NOW() here.
           sleep (2);

           // Step 2 - Teminate Auction
           check_auction_end($intAuctionID );  
         }
      }   
      elseif($_GET["wpa_action"] == "delete") {
         $intAuctionID = $_GET["wpa_id"];

         // check that inputs are numeric         
         if (($intAuctionID > 0)) {
     		    $auction_table_name = $wpdb->prefix . "wpa_auctions";
            $strSQL = "DELETE FROM $auction_table_name WHERE id=" . $intAuctionID;
            $wpdb->query($strSQL);         
         }
      }
      elseif($_GET["wpa_action"] == "resendwinner") {
         $intAuctionID = $_GET["wpa_id"];
         // check that inputs are numeric         
         if (($intAuctionID > 0)) {
            wpa_send_winning_email( $intAuctionID );
         }
      }

   }

   $options = get_option('wp_auctions');
   $currencysymbol = $options['currencysymbol'];

   $nonce = wp_create_nonce ('WPA-nonce')

?>
	<link href="../wp-content/plugins/wp-auctions/requisites/style.css" rel="stylesheet" type="text/css" />

	<div class="wrap wp-auctions wp-auctions-managebids"> 
  		
	<div class="wpa-time"><?php _e('WordPress Time:','wpauctions'); ?> <?php echo get_date_from_gmt(date('Y-m-d H:i:s')); ?></div>

<?php if($_GET["page"] == "wp-auctions-manage") { ?>
	
	<h2 class="manage"><em><?php _e('Current Auctions','wpauctions'); ?></em></h2>
	
	<fieldset class="options">
	<?php
		$table_name = $wpdb->prefix . "wpa_auctions";
		$strSQL = "SELECT id, date_create, date_start, date_end, date_end_original, name, BIN_price, image_url, current_price, reserve_price, terms FROM $table_name WHERE '".current_time('mysql',"1")."' < date_end ORDER BY date_end DESC";
		$rows = $wpdb->get_results ($strSQL);
		
		$bid_table_name = $wpdb->prefix . "wpa_bids";
	?>
	<table class="widefat">
       <thead>
		<tr>
			<th><?php _e('Auction Details','wpauctions'); ?></th>
			<th><?php _e('Created/Ending','wpauctions'); ?></th>
			<th><?php _e('Bids','wpauctions'); ?></th>
			<th><?php _e('Current Price','wpauctions'); ?></th>
			<th><?php _e('Actions','wpauctions'); ?></th>
		</tr>
       </thead>
	<?php if (is_array($rows)): ?>
		<?php foreach ($rows as $row) { 
             $style=" ";
             if($intAlternate==1) $style=$style."alternate "; 
             if(strtotime($row->date_end)<=strtotime("now")) $style=$style."active ";

             ?>
			<tr<?php if($style!=" "): ?> class="<?php echo $style ?>"<?php endif; ?>>
				<td class="wpa-auction-details">
                <h4><?php print $row->name; ?></h4>
                <img src="<?php print wpa_resize($row->image_url,125);  ?>" width="125" height="125">
                <p><?php _e('Category','wpauctions'); ?>: <?php print $row->terms; ?></p>
				        <p><?php _e('ID','wpauctions'); ?>: <?php print $row->id; ?></p>
                </td>
				<td class="wpa-auction-created"><b><?php _e('Created:','wpauctions'); ?></b><br><?php print wpa_date(get_date_from_gmt($row->date_create)); ?> <br>
				    <b><?php _e('Starting','wpauctions'); ?>:</b><br><?php print wpa_date(get_date_from_gmt($row->date_start)); ?><br>
				    <b><?php _e('Ending','wpauctions'); ?>:</b><br><?php print wpa_date(get_date_from_gmt($row->date_end)); ?><br>
            <?php if ( $row->date_end_original != $row->date_end ) { ?>
				    <b><?php _e('Original Ending','wpauctions'); ?>:</b><br><?php print wpa_date(get_date_from_gmt($row->date_end_original)); ?>
				    <?php } ?>
				 </td>
				    
				<td class="wpa-auction-bids">
<?php

  $bids=0;
					// prepare result
	$strSQL = "SELECT id, bidder_name, bidder_email , bidder_url, date,current_bid_price, bid_type FROM $bid_table_name WHERE auction_id=".$row->id." ORDER BY current_bid_price, bid_type DESC";
	$bid_rows = $wpdb->get_results ($strSQL);
			
	foreach ($bid_rows as $bid_row) {
	   echo ('<a href="mailto:'.$bid_row->bidder_email.'">');
	   echo ($bid_row->bidder_name);
	   echo ('</a> ('.$bid_row->bidder_url.') - '.$currencysymbol.$bid_row->current_bid_price);
	   echo ('['.$bid_row->bid_type.']');
	   echo ('<br>');
	   $bids++;
	}		
	
	if ($bids!=0)	{
?>
	   <br>
	   
     <a href="javascript:if(confirm('<?php _e('Are you sure you want to reverse the last bid for','wpauctions'); ?> \'<?php print $bid_row->current_bid_price; ?>\'?')==true) location.href='admin.php?page=wp-auctions-manage&amp;wpa_action=reverse&amp;wpa_id=<?php echo $row->id ?>&amp;bid_id=<?php echo $bid_row->id ?>&amp;_wpnonce=<?php echo $nonce ?>'" class="edit"><?php _e('Cancel Last Bid','wpauctions'); ?></a><br/><br/>
<?php
	}
?>			
          </td>
				<td class="wpa-auction-currentprice"><?php if ( $row->current_price > 0 ) { echo $currencysymbol.$row->current_price; } else { _e('No bids','wpauctions'); }?><?php if ($row->BIN_price>0) print "<br>BIN Price: ".$currencysymbol.$row->BIN_price ?><?php if ($row->reserve_price>0) print "<br>Reserve Price: ".$currencysymbol.$row->reserve_price ?></td>
				<td class="wpa-auction-actions">
            <a href="javascript:if(confirm('<?php _e('Are you sure you want to end auction','wpauctions'); ?> \'<?php print addslashes(str_replace ( '"' , "'" , $row->name)); ?>\'?')==true) location.href='admin.php?page=wp-auctions-manage&amp;wpa_action=terminate&amp;wpa_id=<?php echo $row->id ?>&amp;_wpnonce=<?php echo $nonce ?>'" class="edit"><?php _e('End Auction','wpauctions'); ?></a><br/><br/>
				    <a href="admin.php?page=wp-auctions-add&amp;wpa_action=edit&amp;wpa_id=<?php print $row->id ?>&amp;_wpnonce=<?php echo $nonce ?>" class="edit"><?php _e('Edit','wpauctions'); ?></a><br/><br/>
            <a href="javascript:if(confirm('<?php _e('Delete auction','wpauctions'); ?> \'<?php print addslashes(str_replace ( '"' , "'" , $row->name)); ?>\'? (<?php _e('This will erase all details on bids, winners and the auction','wpauctions'); ?>)')==true) location.href='admin.php?page=wp-auctions-manage&amp;wpa_action=delete&amp;wpa_id=<?php echo $row->id ?>&amp;_wpnonce=<?php echo $nonce; ?>'" class="edit"><?php _e('Delete','wpauctions'); ?></a>
        </td>
			</tr>
			<?php
				if($intAlternate == 1):
					$intAlternate=0;
				else:
					$intAlternate=1;
				endif;
			?>
		<?php } ?>
	<?php else: ?>
		<tr><td colspan="5"><?php _e('No auctions defined','wpauctions'); ?></td></tr>
	<?php endif; ?>
	</table>
	</fieldset>
	
<?php } else { ?>	
	
    <h2 class="manage"><em><?php _e('Closed Auctions','wpauctions'); ?></em></h2>
	<fieldset class="options">
	<?php
		$table_name = $wpdb->prefix . "wpa_auctions";
		$strSQL = "SELECT id, date_create, date_start, date_end, date_end_original, name, image_url, current_price, terms, winner, winner_email, winning_price FROM $table_name WHERE '".current_time('mysql',"1")."' >= date_end ORDER BY date_end";
		$rows = $wpdb->get_results ($strSQL);

		$bid_table_name = $wpdb->prefix . "wpa_bids";
	?>
	<table class="widefat" style="margin: 0 0 10px;">
       <thead>
		<tr>
			<th><?php _e('Auction Details','wpauctions'); ?></th>
			<th><?php _e('Created/Ending','wpauctions'); ?></th>
			<th><?php _e('Bids','wpauctions'); ?></th>
			<th><?php _e('Winner','wpauctions'); ?></th>
			<th><?php _e('Actions','wpauctions'); ?></th>
		</tr>
       </thead>
	<?php if (is_array($rows)): ?>
		<?php foreach ($rows as $row) { 
             $style=" ";
             if($intAlternate==1) $style=$style."alternate "; 
             if(strtotime($row->date_end)<=strtotime("now")) $style=$style."active ";

             ?>
			<tr<?php if($style!=" "): ?> class="<?php echo $style ?>"<?php endif; ?>>
				<td class="wpa-auction-details">
                <h4><?php print $row->name; ?></h4>
                <img src="<?php print wpa_resize($row->image_url,125); ?>" width="125" height="125">
                <p><?php _e('Category','wpauctions'); ?>: <?php print $row->terms; ?></p>
				<p><?php _e('ID:','wpauctions'); ?> <?php print $row->id; ?></p>
                </td>
				<td class="wpa-auction-created"><b><?php _e('Created:','wpauctions'); ?></b><br> <?php print wpa_date(get_date_from_gmt($row->date_create)); ?> <br>
				    <b><?php _e('Started','wpauctions'); ?>:</b><br> <?php print wpa_date(get_date_from_gmt($row->date_start)); ?><br>
				    <b><?php _e('Ended','wpauctions'); ?>:</b><br> <?php print wpa_date(get_date_from_gmt($row->date_end)); ?><br>
            <?php if ( $row->date_end_original != $row->date_end ) { ?>
  				    <b><?php _e('Original Ending','wpauctions'); ?>:</b><br><?php print wpa_date(get_date_from_gmt($row->date_end_original)); ?> 
				    <?php } ?>

				</td>
				<td class="wpa-auction-bids">
				
<?php
					// prepare result
	$strSQL = "SELECT bidder_name, bidder_email ,date,current_bid_price, bid_type FROM $bid_table_name WHERE auction_id=".$row->id." ORDER BY current_bid_price DESC";
	$bid_rows = $wpdb->get_results ($strSQL);
			
	foreach ($bid_rows as $bid_row) {
	   echo ('<a href="mailto:'.$bid_row->bidder_email.'">');
	   echo ($bid_row->bidder_name);
	   echo ('</a> - '.$currencysymbol.$bid_row->current_bid_price);
	   echo ('['.$bid_row->bid_type.']');	   
	   echo ('<br>');
	}		
			
?>
				</td>
				<td class="wpa-auction-currentprice"><p><?php _e('Won by:','wpauctions'); ?> <?php print $row->winner; ?></p>
          <p><?php _e('Winner Email','wpauctions'); ?>: <?php print $row->winner_email; ?></p>
					<p><?php _e('For','wpauctions'); ?>: <?php print $row->winning_price; ?></p>
 
                    </td>
				<td class="wpa-auction-actions">
				    <a href="admin.php?page=wp-auctions-add&amp;wpa_action=relist&amp;wpa_id=<?php print $row->id ?>&amp;_wpnonce=<?php echo $nonce ?>" class="edit"><?php _e('Relist','wpauctions'); ?></a><br/><br/>
            <a href="javascript:if(confirm('<?php _e('Delete auction','wpauctions'); ?> \'<?php print addslashes(str_replace ( '"' , "'" , $row->name)); ?>\'? (<?php _e('This will erase all details on bids, winners and the auction','wpauctions'); ?>)')==true) location.href='admin.php?page=wp-auctions-manage-closed&amp;wpa_action=delete&amp;wpa_id=<?php echo $row->id; ?>&amp;_wpnonce=<?php echo $nonce ?>'" class="edit"><?php _e('Delete','wpauctions'); ?></a><br/><br/>
            <a href="javascript:if(confirm('<?php _e('Resend winner email for','wpauctions'); ?> \'<?php print addslashes(str_replace ( '"' , "'" , $row->name)); ?>\'? (<?php _e('Click NO if the bidder has not requested another copy of the email','wpauctions'); ?>)')==true) location.href='admin.php?page=wp-auctions-manage-closed&amp;wpa_action=resendwinner&amp;wpa_id=<?php echo $row->id; ?>&amp;_wpnonce=<?php echo $nonce ?>'" class="edit"><?php _e('Resend Winner Email','wpauctions'); ?></a></td>
			</tr>
			<?php
				if($intAlternate == 1):
					$intAlternate=0;
				else:
					$intAlternate=1;
				endif;
			?>
		<?php } ?>
	<?php else: ?>
		<tr><td colspan="5"><?php _e('No auctions defined','wpauctions'); ?></td></tr>
	<?php endif; ?>
	</table>
	</fieldset>
	
<?php } ?>

</div>

<?php   
}

function wp_auctions_email() {

   $options = get_option('wp_auctions');
   $requesttobid = $options['requesttobid'];

   // Note: Options for this plugin include a "Title" setting which is only used by the widget
   $options = get_option('wp_auctions_email');
    	
   //set initial values if none exist
   if ( !is_array($options) ) {
      $options = array( 'outbid'=>'', 'win'=>'', 'reserve'=>'', 'watch'=>'',  'cleartobid'=>'');
   }

   if ( $_POST['wp_auctions-submit'] ) {

      // security check
      check_admin_referer( 'WPA-nonce');

      $options['outbid'] = stripslashes($_POST['wpa-outbid']);
      $options['win'] = stripslashes($_POST['wpa-win']);
      $options['reserve'] = stripslashes($_POST['wpa-reserve']);
      $options['watch'] = stripslashes($_POST['wpa-watch']);
      $options['cleartobid'] = stripslashes($_POST['wpa-cleartobid']);
      
      update_option('wp_auctions_email', $options);
   }

   $txtOutBid = $options['outbid'];
   $txtWin = $options['win'];
   $txtReserve = $options['reserve'];
   $txtWatch = $options['watch'];
   $txtClearToBid = $options['cleartobid'];
	
?>

<link href="../wp-content/plugins/wp-auctions/requisites/style.css" rel="stylesheet" type="text/css" />

<div class="wrap wp-auctions">
    
  <form name="form1" method="post" action="<?php admin_url('admin.php?page=page=wp-auctions-email'); ?>">
  
  <?php wp_nonce_field('WPA-nonce'); ?>

  <h2 class="settings"><em><?php _e('Custom Message Settings','wpauctions') ?></em></h2>

    <table width="100%" cellspacing="2" cellpadding="5" class="widefat" style="margin-top: 1em;"> 
     <tr valign="top" class="alternate">
     <th scope="row" class='row-title'><?php _e('Message Options:','wpauctions'); ?></th> 
     <td>
     <p><strong>{site_name}</strong> - <?php _e('The name of your auction site','wpauctions'); ?></p>
     <p><strong>{auction_name}</strong> - <?php _e('The name of the auction this message relates to','wpauctions'); ?></p>
     <p><strong>{auction_link}</strong> - <?php _e('Link back to the auction about which the email is being sent','wpauctions'); ?></p>
     <p><strong>{current_price}</strong> - <?php _e('Current price of the auction about which the email is being sent','wpauctions'); ?></p>
     <p><strong>{reserve_price}</strong> - <?php _e('Reserve price of the auction about which the email is being sent','wpauctions'); ?></p>
     <p><strong>{payment_details}</strong> - <?php _e('Details of how the payment is to be made','wpauctions'); ?></p>
     <p><strong>{contact_email}</strong> - <?php _e('Your contact email address','wpauctions'); ?></p>
     </td>
	</tr>
      <tr valign="top" class="alternate"> 
        <th scope="row" class='row-title'><?php _e('Auction outbid notice:','wpauctions') ?></th> 
        <td>
        <?php
        		$args = array("textarea_name" => "wpa-outbid");
						wp_editor( $txtOutBid, "wpa-outbid", $args );
        ?>
        <!-- <textarea rows="5" cols="60" name="wpa-outbid"><?php print $txtOutBid; ?></textarea> -->
        <br />
        <p><?php _e('If you want a custom message to use when a bidder is outbid, please enter it here. You can use the keywords:<br><strong>{site_name}, {auction_name}, {auction_link}, {current_price}','wpauctions') ?></p></td> 
      </tr> 
      <tr valign="top"> 
        <th scope="row" class='row-title'><?php _e('Auction win notice:','wpauctions') ?></th> 
        <td>
        <?php
        		$args = array("textarea_name" => "wpa-win");
						wp_editor( $txtWin, "wpa-win", $args );
        ?>        
        <!-- <textarea rows="5" cols="60" name="wpa-win"><?php print $txtWin; ?></textarea> -->
        <br />
        <p><?php _e('If you want a custom message to use when a bidder wins an auction, please enter it here. You can use the keywords:<br><strong>{site_name}, {auction_name}, {auction_link}, {current_price}, {payment_details}, {contact_email}','wpauctions') ?></p></td> 
      </tr> 
      <tr valign="top"> 
        <th scope="row" class='row-title'><?php _e('Auction win, but below reserve notice:','wpauctions') ?></th> 
        <td>
        <?php
        		$args = array("textarea_name" => "wpa-reserve");
						wp_editor( $txtReserve, "wpa-reserve", $args );
        ?>
        <!-- <textarea rows="5" cols="60" name="wpa-reserve"><?php print $txtReserve; ?></textarea> -->
        <br />
        <p><?php _e('If you want a custom message to use when a bidder wins an auction, but does not reach Reserve price, please enter it here. You can use the keywords:<br><strong>{site_name}, {auction_name}, {auction_link}, {current_price}, {reserve_price}, {payment_details}, {contact_email}','wpauctions') ?></p></td> 
      </tr> 
      <tr valign="top" class="alternate"> 
        <th scope="row" class='row-title' style="border-bottom: 0;"><?php _e('Auction watch notice:','wpauctions') ?></th> 
        <td style="border-bottom: 0;">
        <?php
        		$args = array("textarea_name" => "wpa-watch");
						wp_editor( $txtWatch, "wpa-watch", $args );
        ?>        
        <!-- <textarea rows="5" cols="60" name="wpa-watch"><?php print $txtWatch; ?></textarea> -->
        <br />
        <p><?php _e('If you want a custom message to use when a watched auction changes, please enter it here. You can use the keywords:<br><strong>{site_name}, {auction_name}, {auction_link}, {current_price}','wpauctions') ?></p></td> 
      </tr>       

<?php   if ($requesttobid == "Yes") { ?>

      <tr valign="top" class="alternate"> 
        <th scope="row" class='row-title' style="border-bottom: 0;"><?php _e('Cleared to Bid Notice:','wpauctions') ?></th> 
        <td style="border-bottom: 0;">
        <?php
        		$args = array("textarea_name" => "wpa-cleartobid");
						wp_editor( $txtClearToBid, "wpa-cleartobid", $args );
        ?>        
        <!-- <textarea rows="5" cols="60" name="wpa-watch"><?php print $txtWatch; ?></textarea> -->
        <br />
        <p><?php _e('If you want a custom message to use when a user recieved notification that he can bid, please enter it here. You can use the keywords:<br><strong>{site_name}, {auction_name}, {auction_link}','wpauctions') ?></p></td> 
      </tr>       

<?php    } ?>

    </table>

	<input type="hidden" id="wp_auctions-submit" name="wp_auctions-submit" value="1" />

    <p class="submit">
      <input type="submit" name="Submit" value="<?php _e('Update Options','wpauctions'); ?> &raquo;" />
    </p>
  </form> 
</div>

<?php
}

function wp_auctions_templates() {

   global $wpdb;

   if ($_GET["wpa_action"] == "modify") {
      $strTemplateName = $_POST["templatename"];
      $intTemplateID = $_GET["wpa_id"];
 
      // check if we need to create the template first
      if ($strTemplateName) {
         $table_name = $wpdb->prefix . "wpa_auctiontemplates";
         $wpdb->insert( 
            $table_name, 
            array( 
              'templatename' => $strTemplateName, 
            ), 
            array( 
              '%s', 
            ) 
          );
          $intTemplateID = $wpdb->insert_id;
      }
 
      // check that inputs are numeric         
      if (($intTemplateID <= 0)) {
        exit("No template ID specified");
      }
    
      $table_name = $wpdb->prefix . "wpa_auctiontemplates";
      $strSQL = "SELECT * FROM $table_name WHERE id=".$intTemplateID;
      $row = $wpdb->get_row ($strSQL);
   
?>

<link href="../wp-content/plugins/wp-auctions/requisites/style.css" rel="stylesheet" type="text/css" />

<div class="wrap wp-auctions wpa-templates">
    
  <h2 class="settings settings-nm"><em><?php _e('Amend Template','wpauctions') ?></em></h2>
    <table width="100%" cellspacing="2" cellpadding="5" class="widefat" style="margin-top: 1em;"> 
    <tr>
     <td>
       <strong><?php _e('Template name','wpauctions') ?>: </strong><?php echo $row->templatename; ?> 
     </td>
    </tr>
    <tr><td>
   	<script type="text/javascript">
		jQuery(function(){
			jQuery('#form-builder').formbuilder({
				'save_url': '<?php echo WPA_PLUGIN_FULL_PATH.'?form_save'; ?>',
				'load_url': '<?php echo WPA_PLUGIN_FULL_PATH.'?form_load&form_id='.$intTemplateID; ?>'
			});
		});
	</script>

  <div id="form-builder"></div>
  <div id="formBuilderReturn">
	</td></tr></table>
</div>
	
<?php	
    } elseif ($_GET["wpa_action"] == "create") {
?>
<link href="../wp-content/plugins/wp-auctions/requisites/style.css" rel="stylesheet" type="text/css" />

<div class="wrap wp-auctions wpa-templates">
    
  <h2 class="settings settings-nm"><em><?php _e('Create Template','wpauctions') ?></em></h2>
  
    <form action="admin.php?page=wp-auctions-templates&wpa_action=modify" method="POST">
    <table width="100%" cellspacing="2" cellpadding="5" class="widefat" style="margin-top: 1em;"> 
    <tr>
     <td>
       <strong><?php _e('Template name','wpauctions') ?>: </strong> <input name="templatename" id="templatename" type="text">
     </td>
    </tr>
   </table>
   <input type="submit" value="<?php _e('Save Template Name','wpauctions') ?>" class="save-template-name">
   </form>
</div>

<?php
    } else {

     // on-screen functions
     if ($_GET["wpa_action"] == "delete") {
       $intTemplateID = $_GET["wpa_id"];

       // check that inputs are numeric         
       if (($intTemplateID > 0)) {
          $table_name = $wpdb->prefix . "wpa_auctiontemplates";
          $strSQL = "DELETE FROM $table_name WHERE id=" . $intTemplateID;
          $wpdb->query($strSQL);         
       }
    }

// show default screen
?>
<link href="../wp-content/plugins/wp-auctions/requisites/style.css" rel="stylesheet" type="text/css" />

<div class="wrap wp-auctions wpa-templates">
    
  <h2 class="settings settings-nm"><em><?php _e('Auction Templates','wpauctions') ?></em></h2>
    <table width="100%" cellspacing="2" cellpadding="5" class="widefat" style="margin-top: 1em;"> 

<?php
		$table_name = $wpdb->prefix . "wpa_auctiontemplates";
		$strSQL = "SELECT * FROM $table_name";
		$rows = $wpdb->get_results ($strSQL);
?>

	<?php if (is_array($rows)): ?>
		<?php foreach ($rows as $row) { ?>

      <tr valign="top" class="alternate"> 
        <td>
         <?php echo $row->templatename; ?>
        </td>
        <td>
           <a href="admin.php?page=wp-auctions-templates&wpa_action=modify&wpa_id=<?php echo $row->id; ?>"><?php _e('Edit','wpauctions') ?></a>  &nbsp; 
           <a href="admin.php?page=wp-auctions-templates&wpa_action=delete&wpa_id=<?php echo $row->id; ?>"><?php _e('Delete','wpauctions') ?></a>
        </td>
      <tr>
      
     <?php } ?>
  <?php endif; ?>  
    <table>

    <div class="add-template"><a href="admin.php?page=wp-auctions-templates&wpa_action=create"><?php _e('Add a new template','wpauctions') ?></a></div>

<?php
   }
}

function wp_auctions_design() {

   global $wpdb;
      
   // Note: Options for this plugin include a "Title" setting which is only used by the widget
   $options = get_option('wp_auctions_design');
	
   //set initial values if none exist
   if ( !is_array($options) ) {
      $options = array( 'auctioncolorone'=>'#d94426','auctioncolortwo'=>'#ffffff','auctioncolorthree'=>'#eaeada','auctioncolorfour'=>'#bcaa97','auctioncolorfive'=>'#59362b','DoNotCrop'=>'');
   }

   if ( $_POST['wp_auctions-submit'] ) {

      // security check
      check_admin_referer( 'WPA-nonce');

      $options['auctioncolorone'] = strip_tags(stripslashes($_POST['wpa-auctioncolorone']));
      $options['auctioncolortwo'] = strip_tags(stripslashes($_POST['wpa-auctioncolortwo']));
      $options['auctioncolorthree'] = strip_tags(stripslashes($_POST['wpa-auctioncolorthree']));
      $options['auctioncolorfour'] = strip_tags(stripslashes($_POST['wpa-auctioncolorfour']));
      $options['auctioncolorfive'] = strip_tags(stripslashes($_POST['wpa-auctioncolorfive']));
      $options['DoNotCrop'] = strip_tags(stripslashes($_POST['wpa-DoNotCrop']));

      update_option('wp_auctions_design', $options);
   }

   $auctioncolorone = htmlspecialchars($options['auctioncolorone'], ENT_QUOTES);
   $auctioncolortwo = htmlspecialchars($options['auctioncolortwo'], ENT_QUOTES);
   $auctioncolorthree = htmlspecialchars($options['auctioncolorthree'], ENT_QUOTES);
   $auctioncolorfour = htmlspecialchars($options['auctioncolorfour'], ENT_QUOTES);
   $auctioncolorfive = htmlspecialchars($options['auctioncolorfive'], ENT_QUOTES);
   $DoNotCrop = htmlspecialchars($options['DoNotCrop'], ENT_QUOTES);
	
   // implement fudge for people upgrading having blank values and fabtastic doesn't handle blank fields very well
   if ( $auctioncolorone == "" ) $auctioncolorone = "#d94426";
   if ( $auctioncolortwo == "" ) $auctioncolortwo = "#ffffff";
   if ( $auctioncolorthree == "" ) $auctioncolorthree = "#eaeada";	
   if ( $auctioncolorfour == "" ) $auctioncolorfour = "#bcaa97";
   if ( $auctioncolorfive == "" ) $auctioncolorfive = "#59362b";	
	
?>

<link href="../wp-content/plugins/wp-auctions/requisites/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
  jQuery(document).ready(function() {


   // Initialise Colour Picker
        var f = jQuery.farbtastic('#picker');
        var p = jQuery('#picker').css('opacity', 0.25);
        var selected;
        jQuery('.colorwell')
          .each(function () { f.linkTo(this); jQuery(this).css('opacity', 0.75); })
          .focus(function() {
            if (selected) {
              jQuery(selected).css('opacity', 0.75).removeClass('colorwell-selected');
            }
            f.linkTo(this);
            p.css('opacity', 1);
            jQuery(selected = this).css('opacity', 1).addClass('colorwell-selected');
          });

    jQuery('#reset').click(function() {
      jQuery(':input').each(function() {
         if (this.type == 'text')
            this.value = "";
      });
      jQuery('#form1').submit();
    });

  });

</script>

<div class="wrap wp-auctions wpa-design">

  <form id="form1" name="form1" method="post" action="<?php admin_url('admin.php?page=page=wp-auctions-design'); ?>">
  
  <?php wp_nonce_field('WPA-nonce'); ?>
  
  <h2 class="settings settings-nm"><em><?php _e('Design Settings','wpauctions') ?></em></h2>

    <table width="100%" cellspacing="2" cellpadding="5" class="widefat" style="margin-top: 1em;"> 
      <tr valign="top" class="alternate"> 
        <th scope="row" class='row-title'><?php _e('Link Colors 1 and 2','wpauctions') ?><br /><small><?php _e('Default: #d94426 and #ffffff','wpauctions') ?></small></th> 
        <td class='desc'>
        	<div class="clearfix">
       	 		<div class="picker-left">
                	<p><strong>1. <?php _e('Buttons, Links and Current Bid color:','wpauctions') ?></strong></p>
			        <p><em><?php _e('This is the main link color, it should be dark or bright.','wpauctions') ?></em></p>
                    <input name="wpa-auctioncolorone" class="colorwell" type="text" id="wpa-auctioncolorone" value="<?php echo $auctioncolorone; ?>" />
                    
		        	<p><strong>2. <?php _e('Buttons text color and Title color','wpauctions') ?></strong></p>
	        		<p><em><?php _e('This must contrast Light Color 2','wpauctions') ?></em></p>
                    <input name="wpa-auctioncolortwo" class="colorwell" type="text" id="wpa-auctioncolortwo" value="<?php echo $auctioncolortwo; ?>" />
                    
              	</div>
		        <div id="picker"></div>
    	    </div>
        </td>
      </tr> 
      <tr valign="top"> 
        <th scope="row" class='row-title'><?php _e('Light Color 1','wpauctions') ?><br /><small><?php _e('Default: #eaeada','wpauctions') ?></small></th> 
        <td class='desc'>
        <p><strong><?php _e('Form background and Watch button:','wpauctions') ?></strong></p>
        <p><em><?php _e('This has to be a lighter shade than Light Color 2','wpauctions') ?></em></p>
        <input name="wpa-auctioncolorthree" class="colorwell" type="text" id="wpa-auctioncolorthree" value="<?php echo $auctioncolorthree; ?>" />
        
        </td> 
      </tr>
      <tr valign="top" class="alternate"> 
        <th scope="row" class='row-title'><?php _e('Light Color 2','wpauctions') ?><br /><small><?php _e('Default: #bcaa97','wpauctions') ?></small></th> 
        <td class='desc'>
        <p><strong><?php _e('Borders, Form borders, Title backgrounds, Image backgrounds and Dormant text color:','wpauctions') ?></strong></p>
        <p><em><?php _e('This has to be a few shades darker than Light Color 1 but considerably lighter than Dark Color 1','wpauctions') ?></em></p>
        <input name="wpa-auctioncolorfour" class="colorwell" type="text" id="wpa-auctioncolorfour" value="<?php echo $auctioncolorfour; ?>" />
        
        </td> 
      </tr>
      <tr valign="top" class="alternate"> 
        <th scope="row" class='row-title' style="border-bottom: 0;"><?php _e('Dark Color 1','wpauctions') ?><br /><small><?php _e('Default: #59362b','wpauctions') ?></small></th> 
        <td class='desc' style="border-bottom: 0;">
        <p><strong><?php _e('Main text, Image thumbnails border color:','wpauctions') ?></strong></p>
        <p><em><?php _e('Has to be quite darker than Light Color 2 and must contrast Link Color 1','wpauctions') ?></em></p>    
        <input name="wpa-auctioncolorfive" class="colorwell" type="text" id="wpa-auctioncolorfive" value="<?php echo $auctioncolorfive; ?>" />    
        
        </td> 
      </tr>

    </table>

  <h2 class="settings settings-nm"><em><?php _e('Image Options','wpauctions') ?></em></h2>

    <table width="100%" cellspacing="2" cellpadding="5" class="widefat" style="margin-top: 1em;"> 
      <tr valign="top" class="alternate"> 
        <th scope="row" class='row-title' style="border-bottom: 0;"><?php _e('Do Not Crop','wpauctions') ?></th> 
        <td class='desc' style="border-bottom: 0;">
        <select id="wpa-DoNotCrop" name="wpa-DoNotCrop">
                <option value="" <?php if ($DoNotCrop=='') echo 'selected'; ?>><?php _e('Crop images when resizing (default)','wpauctions') ?></option>
                <option value="Yes" <?php if ($DoNotCrop=='Yes') echo 'selected'; ?>><?php _e('Do Not Crop','wpauctions') ?></option>
         </select>
        <br />
        <p><?php _e('Turn off cropping behaviour (note: this may change the way your image looks)','wpauctions') ?></p></td> 
      </tr> 
    </table>

	<input type="hidden" id="wp_auctions-submit" name="wp_auctions-submit" value="1" />

    <p class="submit">
      <input type="button" name="Reset" value="<?php _e('Reset Colors to Defaults','wpauctions') ?> &raquo;" id="reset" />
      <input type="submit" name="Submit" value="<?php _e('Update Settings','wpauctions') ?> &raquo;" />
    </p>
  </form> 
</div>

<?php
}

function wpa_admin_notice(){

    global $wpdb;
    $options = get_option('wp_auctions');
    
    $errors = "";

    // check we have an engine defined
    $engine = $options['engine'];
    if ($engine < 1)
       $errors .= "<li>WP Auctions hasn't been configured yet.</li>";

    // check we have an increment
    $customincrement = htmlspecialchars($options['customincrement'], ENT_QUOTES);
    if ($customincrement != "")
       if (floatval($customincrement) < 0.01 )
          $errors .= "<li>Invalid increment value</li>";
       
    if ($errors != "") {
      echo '<div class="error">';
      echo "We're sorry to bug you, but we've found the following problems with WP Auctions:";
      echo "<ul>";
      echo $errors;
      echo "</ul>";
      echo '</div>';
    }
}
add_action('admin_notices', 'wpa_admin_notice');


?>