<?php header("Content-type: text/css"); 

// hook into Wordpress and pull out the options
if (!function_exists('get_option')) 
   require_once('../../../../wp-config.php'); 

$options = get_option('wp_auctions_design');

$auctioncolorone = htmlspecialchars($options['auctioncolorone'], ENT_QUOTES);
$auctioncolortwo = htmlspecialchars($options['auctioncolortwo'], ENT_QUOTES);
$auctioncolorthree = htmlspecialchars($options['auctioncolorthree'], ENT_QUOTES);
$auctioncolorfour = htmlspecialchars($options['auctioncolorfour'], ENT_QUOTES);
$auctioncolorfive = htmlspecialchars($options['auctioncolorfive'], ENT_QUOTES);
?>

/*

Sidebar Widget
--------------

*/

	.wpa-widget a { color: <?php echo $auctioncolorone; ?> !important; }
	.wpa-widget a:hover { color: <?php echo $auctioncolorfive; ?> !important; }

/* Title */
	.wpa-title { background: <?php echo $auctioncolorfour; ?> !important; }
	.wpa-title p { color: <?php echo $auctioncolortwo; ?> !important; }

/* Body */
	.wpa-body { <?php if (strlen($auctioncolorfour) > 0) { ?> border: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> }

/* Featured */
	.wpa-featured h3 { color: <?php echo $auctioncolorfive; ?> !important; }

/* Current Bid */
    p.wpa-currentbid { color: <?php echo $auctioncolorfive; ?> !important; }
    p.wpa-winning, p.wpa-reserve { color: <?php echo $auctioncolorfour; ?> !important; }

/* Description */
	.wpa-widgetdescription p { color: <?php echo $auctioncolorfive; ?> !important; }

/* Bidding */	
	p.wpa-bidnow a { background: <?php echo $auctioncolorone; ?> !important; }
	p.wpa-endtime { color: <?php echo $auctioncolorfive; ?> !important; }
	
	.wpa-widget p.wpa-bin a { background: <?php echo $auctioncolorone; ?> !important; }
	
	.wpa-widget p.wpa-bidnow a, .wpa-widget p.wpa-bin a { color: <?php echo $auctioncolortwo; ?> !important; }
	.wpa-widget p.wpa-bidnow a:hover, .wpa-widget p.wpa-bin a:hover { background: <?php echo $auctioncolorfive; ?> !important; }

/* Watch Auction */
	.wpa-watch { <?php if (strlen($auctioncolorthree) > 0) { ?> border: 1px solid <?php echo $auctioncolorthree; ?> !important; <?php } ?> }
	.wpa-watch input[type=text] { background: <?php echo $auctioncolorthree; ?> !important; <?php if (strlen($auctioncolorfour) > 0) { ?> border-left: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> <?php if (strlen($auctioncolorfour) > 0) { ?> border-top: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> color: <?php echo $auctioncolorfour; ?> !important; }
	.wpa-watch input[type=text]:focus { color: <?php echo $auctioncolorfive; ?> !important; }
	
	.wpa-watch input[type=button] { color: <?php echo $auctioncolorfour; ?> !important; }
	.wpa-watch input[type=button]:hover { color: <?php echo $auctioncolorone; ?> !important; }
	.wpa-watch label { color: <?php echo $auctioncolorfive; ?> !important; }

/* Other Auctions */
	#wpa-other h4 { color: <?php echo $auctioncolorfive; ?> !important; }
	#wpa-other h4.wpa-current { <?php if (strlen($auctioncolorfour) > 0) { ?> border-bottom: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> }
	
	.wpa-widget #wpa-other .wpa-other-item a { color: <?php echo $auctioncolorfive; ?> !important; text-decoration: none !important; }
	.wpa-widget #wpa-other .wpa-other-item a:hover { color: <?php echo $auctioncolorone; ?> !important; }
	
	#wpa-other .wpa-other-item p { <?php if (strlen($auctioncolorthree) > 0) { ?> border-bottom: 1px solid <?php echo $auctioncolorthree; ?> !important; <?php } ?> }
	
/* Form ToolTip */
	.wpa-tooltip { background-color: <?php echo $auctioncolorone; ?>; <?php if (strlen($auctioncolorone) > 0) { ?> border: 1px solid <?php echo $auctioncolorone; ?> !important; <?php } ?> color: <?php echo $auctioncolortwo; ?>; -moz-box-shadow: 0 0 5px <?php echo $auctioncolorfive; ?>; -webkit-box-shadow: 0 0 5px <?php echo $auctioncolorfive; ?>; }

/* RSS */
	.wpa-rss a { color: <?php echo $auctioncolorone; ?>; }

/* List View */
	.wpa-other-list { <?php if (strlen($auctioncolorthree) > 0) { ?> border: 1px solid <?php echo $auctioncolorthree; ?> !important; <?php } ?> }
	.wpa-other-list .wpa-other-item p { <?php if (strlen($auctioncolorthree) > 0) { ?> border-bottom: 1px solid <?php echo $auctioncolorthree; ?> !important; <?php } ?> }

/*

Popup Widget
------------

*/

	.wpa-popup { <?php if (strlen($auctioncolorfour) > 0) { ?> border: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> }

/* Title */
	.wpa-popup .wpa-title p { color: <?php echo $auctioncolorfive; ?> !important; }

	.wpa-popup .wpa-title { <?php if (strlen($auctioncolorthree) > 0) { ?> border-bottom: 1px solid <?php echo $auctioncolorthree; ?> !important; <?php } ?> }
	p.wpa-title-extra a { color: <?php echo $auctioncolorfour; ?>; }
	p.wpa-title-extra a:hover { color: <?php echo $auctioncolorfive; ?>; }

/* Top Left Image Tabs */
	.wpa-top-left .wpa-tabs-container { background: <?php echo $auctioncolortwo; ?>; }
	
	/* Tabs */
	.wpa-top-left .wpa-tabs { background: <?php echo $auctioncolorfour; ?>; <?php if (strlen($auctioncolortwo) > 0) { ?> border-top: 1px solid <?php echo $auctioncolortwo; ?> !important; <?php } ?> }
	.wpa-top-left .wpa-tabs img { <?php if (strlen($auctioncolorfive) > 0) { ?> border: 1px solid <?php echo $auctioncolorfive; ?> !important; <?php } ?> }
	.wpa-top-left .wpa-tabs a.current img { <?php if (strlen($auctioncolorone) > 0) { ?> border: 1px solid <?php echo $auctioncolorone; ?> !important; <?php } ?> }

/* Top Right */
	.wpa-top-right { background: <?php echo $auctioncolorthree; ?>; }
	.wpa-top-right h3 { color: <?php echo $auctioncolorone; ?>; }
	
	/* Tabs */
	.wpa-top-right .wpa-tabs a { background: <?php echo $auctioncolortwo; ?>; <?php if (strlen($auctioncolortwo) > 0) { ?> border: 1px solid <?php echo $auctioncolortwo; ?> !important; <?php } ?> <?php if (strlen($auctioncolorfour) > 0) { ?> border-left: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> <?php if (strlen($auctioncolorfour) > 0) { ?> border-top: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> color: <?php echo $auctioncolorfive; ?>; }
	.wpa-top-right .wpa-tabs a:hover { <?php if (strlen($auctioncolortwo) > 0) { ?> border: 1px solid <?php echo $auctioncolortwo; ?> !important; <?php } ?> <?php if (strlen($auctioncolorfour) > 0) { ?> border-bottom: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> <?php if (strlen($auctioncolorfour) > 0) { ?> border-right: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> }
	.wpa-top-right .wpa-tabs a.current { background: <?php echo $auctioncolorone; ?>; <?php if (strlen($auctioncolorone) > 0) { ?> border: 1px solid <?php echo $auctioncolorone; ?> !important; <?php } ?> color: <?php echo $auctioncolortwo; ?> !important; }

	/* Input Fields */
	.wpa-top-right label { color: <?php echo $auctioncolorfive; ?>; }
	.wpa-top-right input { background: <?php echo $auctioncolorthree; ?>; <?php if (strlen($auctioncolorfour) > 0) { ?> border: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> color: <?php echo $auctioncolorfive; ?>; }
	.wpa-top-right input:focus { color: <?php echo $auctioncolorfive; ?>; }

	/* Current Bid */
	.wpa-top-right p.wpa-currentbid { color: <?php echo $auctioncolorone; ?> !important; }
	.wpa-top-right .wpa-buyitnow p.wpa-currentbid { padding: 10px 0 !important; }
	.wpa-top-right p.wpa-currentbid span { color: <?php echo $auctioncolorfour; ?> !important; }
	.wpa-top-right p.wpa-currentbid span#wpa-currentbid { color: <?php echo $auctioncolorone; ?> !important; }

	/* Maximum Bid */
	.wpa-top-right p.wpa-maximumbid { color: <?php echo $auctioncolorfive; ?> !important; }

	/* Bid Amount */
	.wpa-top-right p.wpa-bidamount { color: <?php echo $auctioncolorfive; ?> !important; }
	.wpa-top-right p.wpa-bidamount a, .wpa-top-right p.wpa-bidamount span.wpa-refresh a:hover { color: <?php echo $auctioncolorone; ?> !important; }
	.wpa-top-right p.wpa-bidamount span.wpa-refresh a { color: <?php echo $auctioncolorfive; ?> !important; }
	.wpa-top-right p.wpa-bidamount a:hover { color: <?php echo $auctioncolorfive; ?> !important; }

	#wpa-placebid, #wpa-buyitnowbin { background: <?php echo $auctioncolorone; ?> !important; border: 0; color: <?php echo $auctioncolortwo; ?> !important; }
	#wpa-placebid:hover, #wpa-buyitnowbin:hover { background: <?php echo $auctioncolorfive; ?> !important; cursor: pointer; }
	
	/* Buy it Now */
	p.wpa-checkemail { color: <?php echo $auctioncolorfive; ?> !important; }

	/* Current Bids */
	.wpa-currentbids ol { color: <?php echo $auctioncolorfive; ?> !important; }
	.wpa-currentbids li { <?php if (strlen($auctioncolorthree) > 0) { ?> border-bottom: 1px solid <?php echo $auctioncolorthree; ?> !important; <?php } ?>}
	.wpa-currentbids a { color: <?php echo $auctioncolorone; ?> !important; }
	.wpa-currentbids a:hover { color: <?php echo $auctioncolorfive; ?> !important; }
	
	/* Description */
	.wpa-description p { color: <?php echo $auctioncolorfive; ?> !important; }
	.wpa-description a { color: <?php echo $auctioncolorone; ?> !important; }
	.wpa-description a:hover { color: <?php echo $auctioncolorfive; ?> !important; }

	/* Description Item Details */}
    .wpa-top-right .wpa-itemdetails li { color: <?php echo $auctioncolorfive; ?> !important; }
	.wpa-top-right .wpa-itemdetails li span { color: <?php echo $auctioncolorfour; ?> !important; }

/* Bottom Container */

	.wpa-bottom-container h4 { <?php if (strlen($auctioncolorone) > 0) { ?> border-bottom: 1px solid <?php echo $auctioncolorone; ?> !important; <?php } ?> color: <?php echo $auctioncolorone; ?> !important; }

/* Bottom Left */

	/* Popup Other Auctions */
	.wpa-bottom-left .wpa-otherauctions p a { color: <?php echo $auctioncolorfive; ?> !important; }
	.wpa-bottom-left .wpa-otherauctions p a:hover { color: <?php echo $auctioncolorone; ?> !important; }
	
	.wpa-bottom-left .wpa-otherauctions p { <?php if (strlen($auctioncolorthree) > 0) { ?> border-bottom: 1px solid <?php echo $auctioncolorthree; ?> !important; <?php } ?> color: <?php echo $auctioncolorfour; ?> !important; }

/* Bottom Right */
	.wpa-bottom-right { <?php if (strlen($auctioncolorleft) > 0) { ?> border-left: 1px solid <?php echo $auctioncolorthree; ?> !important; <?php } ?> }
	
	/* Bottom Watch */
	.wpa-bottom-right label { color: <?php echo $auctioncolorfour; ?> !important; }
	
	.wpa-popup .wpa-bottom-right #wpa-popupemail { background: <?php echo $auctioncolorthree; ?> !important; <?php if (strlen($auctioncolorfour) > 0) { ?> border: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> color: <?php echo $auctioncolorfour; ?>; }
	.wpa-bottom-right #wpa-popupemail:focus { color: <?php echo $auctioncolorfive; ?> !important; }
	.wpa-bottom-right input[type=button] { background: <?php echo $auctioncolorthree; ?> !important; <?php if (strlen($auctioncolorthree) > 0) { ?> border: 1px solid <?php echo $auctioncolorthree; ?> !important; <?php } ?> color: <?php echo $auctioncolorfour; ?> !important; }
	.wpa-bottom-right input[type=button]:hover { background: <?php echo $auctioncolorfour; ?> !important; <?php if (strlen($auctioncolorfour) > 0) { ?> border: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> color: <?php echo $auctioncolorfive; ?> !important; }
	
/*

Auction in a Post
-----------------

*/

/* Post - Title */
	.wpa-post .wpa-title { <?php if (strlen($auctioncolorthree) > 0) { ?> border-bottom: 1px solid <?php echo $auctioncolorthree; ?> !important; <?php } ?> }
	.wpa-post .wpa-title h3 { color: <?php echo $auctioncolorfive; ?> !important; }
    
/* Post - Current Bid */
	.wpa-post-container p.wpa-currentbid { background: <?php echo $auctioncolorone; ?> !important; color: <?php echo $auctioncolortwo; ?> !important; }
	.wpa-post-container p.wpa-currentbid span.wpa-topbidder { color: <?php echo $auctioncolorthree; ?> !important; }

	/* Post - Starting Bid */
	.wpa-post-container p.wpa-startingbid { color: <?php echo $auctioncolorfive; ?> !important; }

	/* Post - Bidding Tabs */
	.wpa-post .wpa-tabs-container { background: <?php echo $auctioncolorthree; ?>; }
	
	/* Post - Tabs */
	.wpa-post-container .wpa-tabs a { background: <?php echo $auctioncolorthree; ?>; <?php if (strlen($auctioncolortwo) > 0) { ?> border: 1px solid <?php echo $auctioncolortwo; ?> !important; <?php } ?> <?php if (strlen($auctioncolorfour) > 0) { ?> border-right: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> <?php if (strlen($auctioncolorfour) > 0) { ?> border-bottom: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> color: <?php echo $auctioncolorfive; ?>; }
	.wpa-post-container .wpa-tabs a:hover { <?php if (strlen($auctioncolortwo) > 0) { ?> border: 1px solid <?php echo $auctioncolortwo; ?> !important; <?php } ?> <?php if (strlen($auctioncolorfour) > 0) { ?> border-left: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> <?php if (strlen($auctioncolorfour) > 0) { ?> border-top: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> }
	.wpa-post-container .wpa-tabs a.current { background: <?php echo $auctioncolorone; ?>; <?php if (strlen($auctioncolorone) > 0) { ?> border: 1px solid <?php echo $auctioncolorone; ?> !important; <?php } ?> color: <?php echo $auctioncolortwo; ?>; }

	/* Post - Input Fields */
	.wpa-post-container label { color: <?php echo $auctioncolorfive; ?>; }
	.wpa-post-container input { background: <?php echo $auctioncolorthree; ?>; <?php if (strlen($auctioncolorfour) > 0) { ?> border-left: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> <?php if (strlen($auctioncolorfour) > 0) { ?> border-top: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> color: <?php echo $auctioncolorfive; ?> !important; }
	.wpa-post-container input:focus { color: <?php echo $auctioncolorfive; ?> !important; }

	/* Post - Maximum Bid, Reserve and Ending */
	.wpa-post-container p.wpa-maximumbid, .wpa-post-container p.wpa-reserve, .wpa-post-container p.wpa-post-ending { color: <?php echo $auctioncolorfive; ?> !important; }
	.wpa-post-container p.wpa-maximumbid a { color: <?php echo $auctioncolorone; ?> !important; }
	.wpa-post-container p.wpa-maximumbid a:hover { color: <?php echo $auctioncolorfive; ?> !important; }
	.wpa-post-container p.wpa-post-ending { background: <?php echo $auctioncolorone; ?> !important; color: <?php echo $auctioncolortwo; ?> !important; }
	
	/* Post - Buy this item */
	.wpa-post-container p.wpa-buythisitem, .wpa-post-container p.wpa-checkemail { color: <?php echo $auctioncolorfive; ?> !important; }

	/* Post - Place Bid + Buy it Now */
	.wpa-post-container #wpa-placebid, .wpa-post-container #wpa-buyitnowbin { background: <?php echo $auctioncolorone; ?> !important; border: 0; color: <?php echo $auctioncolortwo; ?> !important; }
	.wpa-post-container #wpa-placebid:hover, .wpa-post-container #wpa-buyitnowbin:hover { background: <?php echo $auctioncolorfive; ?> !important; }
	.wpa-post-container #wpa-currencybuyitnow { color: <?php echo $auctioncolorfive; ?> !important; }

	/* Post - Current Bids */
	.wpa-post-container .wpa-currentbids { background: <?php echo $auctioncolortwo; ?>; }
    
    /* Post - Watch */
	.wpa-post-container .wpa-watchtab p { color: <?php echo $auctioncolorfive; ?> !important; }
	
	/* Post - Manual Refresh */
	p.wpa-refresh-bids-link a { color: <?php echo $auctioncolorone; ?>; }
	p.wpa-refresh-bids-link a:hover { color: <?php echo $auctioncolorfive; ?>; }
	
	/* Post - Shipping Payment */
	.wpa-post-shipping-payment { background: <?php echo $auctioncolorthree; ?>; }
	.wpa-post-shipping h4, .wpa-post-payment h4 { <?php if (strlen($auctioncolorfour) > 0) { ?> border-left: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> <?php if (strlen($auctioncolorfour) > 0) { ?> border-top: 1px solid <?php echo $auctioncolorfour; ?> !important; <?php } ?> color: <?php echo $auctioncolorfive; ?> !important; }
	.wpa-post-shipping p, .wpa-post-payment p { color: <?php echo $auctioncolorfive; ?> !important; }
	
	/* Post - Shipping */
	.wpa-post-shipping h4 { <?php if (strlen($auctioncolortwo) > 0) { ?>background: <?php echo $auctioncolortwo; ?> url(shipping.png) no-repeat 10px center !important;<?php } ?> }
	
	/* Post - Payment */
	.wpa-post-payment h4 { <?php if (strlen($auctioncolortwo) > 0) { ?>background: <?php echo $auctioncolortwo; ?> url(payments.png) no-repeat 10px center !important;<?php } ?> }
	
	/* Post - Auction Closed */
	.wpa-auction-closed-notice { background: <?php echo $auctioncolorthree; ?>; }
	.wpa-auction-closed-notice p { color: <?php echo $auctioncolorfive; ?>; }
	
	/* Logged in to bid */
	p.wpa-loggedin { color: <?php echo $auctioncolorfive; ?> !important; }
	p.wpa-loggedin a { color: <?php echo $auctioncolorone; ?> !important; }
	p.wpa-loggedin a:hover { color: <?php echo $auctioncolorfive; ?> !important; }

	.wpa-popup p.wpa-powered { color: <?php echo $auctioncolorfour; ?>; }
	.wpa-popup p.wpa-powered a { <?php if (strlen($auctioncolorfour) > 0) { ?> border-bottom: 1px dotted <?php echo $auctioncolorfour; ?> !important; <?php } ?> color: <?php echo $auctioncolorfour; ?> !important; }
	.wpa-popup p.wpa-powered a:hover { color: <?php echo $auctioncolorfive; ?> !important; }
    
	/* List Widget */
	.wpa-tables { background: #fff !important; border: 1px solid <?php echo $auctioncolorfour; ?> !important; }
	.wpa-tables th { background: <?php echo $auctioncolorthree; ?>; border: 1px solid <?php echo $auctioncolorfour; ?>; color: <?php echo $auctioncolorfive; ?> !important; }
	.wpa-tables td { border: 0; border-bottom: 1px solid <?php echo $auctioncolorfour; ?>; }
	.wpa-tables td p { color: <?php echo $auctioncolorfive; ?> !important;  }
	.wpa-tables td img { background: <?php echo $auctioncolorthree; ?>; }
	.wpa-tables td a { background: <?php echo $auctioncolorone; ?>; color: <?php echo $auctioncolortwo; ?>; }
	.wpa-tables td a:hover { background: <?php echo $auctioncolorfive; ?>; }
	
	/* List Widget, Sidebar */
	.wp-body-list { background: <?php echo $auctioncolorthree; ?>; border: 1px solid <?php echo $auctioncolorfour; ?>; }
	.wp-head-list h3 { background: <?php echo $auctioncolorfour; ?> !important; color: <?php echo $auctioncolortwo; ?> !important; }
	.wp-heading-list { color: <?php echo $auctioncolorfive; ?>; }
	.wp-desc-list a { color: <?php echo $auctioncolorone; ?> !important; }
	.wp-bidnow-list { background: <?php echo $auctioncolorone; ?> !important; }
	.wp-bidnow-list a, .wp-bidnow-list a:hover { color: <?php echo $auctioncolortwo; ?> !important; }
	.wp-bidnow-list:hover { background: <?php echo $auctioncolorfive; ?> !important; }
	
	#wpa_list_paging_button ul li { background: <?php echo $auctioncolorone; ?> !important; }
	#wpa_list_paging_button ul li:hover { background: <?php echo $auctioncolorfive; ?> !important; }
	#wpa_list_paging_button ul li.wpa-list-widget-current { background: <?php echo $auctioncolorfive; ?> !important; }
	
	/* Timer */
	.hasCountdown { background-color: <?php echo $auctioncolorone; ?> !important; }
	.countdown_section { color: <?php echo $auctioncolorfour; ?> !important; }
	.countdown_amount { color: <?php echo $auctioncolorfive; ?> !important; }
	