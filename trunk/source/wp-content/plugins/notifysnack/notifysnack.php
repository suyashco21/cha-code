<?php

/*
Plugin Name: NotifySnack for WordPress plugin
Plugin URI: http://www.notifysnack.com/
Description: NotifySnack for WordPress plugin
Version: 1.1
Author: Snacktools
Author URI: http://www.snacktools.com
License: GPL2

Copyright 2013 Snacktools
 
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


class NotifySnackPlugin {
    var $namespace = 'notifysnack-plugin';
    var $version = '0.9';
	
    var $defaults = array(
        'notifyscript' => "",
        'notifyscript_location' => 'footer',
        'notifyscript_showon' => 'all',
        'notifyscript_allpages' => 'false',
		'notifyscript_pages' => '',
        'notifyscript_allcats' => 'false',
		'notifyscript_cats' => '',
		'notifyscript_posts' => ''		
    );
    
    function __construct() {
        $this->url_path = WP_PLUGIN_URL . "/" . plugin_basename( dirname( __FILE__ ) );
        
        if( isset( $_SERVER['HTTPS'] ) && (boolean) $_SERVER['HTTPS'] === true ) {
            $this->url_path = str_replace( 'http://', 'https://', $this->url_path );
        }
        
        $this->option_name = '_' . $this->namespace . '--options';
        
        add_action( 'admin_menu', array( &$this, 'admin_menu' ) );
        
        if( is_admin() ) {
        	
            wp_register_style( $this->namespace, $this->url_path . '/css/styles.css', array(), $this->version );
            wp_enqueue_style( $this->namespace );

            wp_register_script( $this->namespace, $this->url_path . '/js/scripts.js', array( 'jquery' ), $this->version );
            wp_enqueue_script( $this->namespace );			
        } else {
        	if( $this->get_option( 'notifyscript_location' ) == 'header' ){
		        add_action( 'wp_head', array( &$this, 'add_notify_script' ) );
        	} else {
			    if( function_exists( 'wp_print_footer_scripts' ) ) {
			        add_action( 'wp_print_footer_scripts', array( &$this, 'add_notify_script' ) );
			    } else {
			        add_action( 'wp_footer', array( &$this, 'add_notify_script' ) );
			    }
        	}
			
        }
    }
    
	function add_notify_script () {
	    $notifyscript = $this->get_option( 'notifyscript' );
		global $post;
		
		if( !empty( $notifyscript ) ) {
			$notifyscript = html_entity_decode( $notifyscript );
            
			if( $this->get_option( 'notifyscript_location' ) == 'header' ) {
				$output = preg_replace( "/<noscript>(.*)<\/noscript>/ism", "", $notifyscript );
			} else {
				$output = $notifyscript;
			}
            
			if( $this->get_option('notifyscript_showon') == 'custom') {
				if(is_page() && $this->get_option('notifyscript_allpages') == 'false') {
					$notifyscript_pages = explode(',', $this->get_option('notifyscript_pages'));
					if(!in_array($post->ID, $notifyscript_pages)) {
						return;
					}
				}

				if(is_single() && $this->get_option('notifyscript_allcats') == 'false' ) {
					$categories = get_the_category();
					$found = false;
					$notifyscript_cats = explode(',', $this->get_option('notifyscript_cats'));					
					foreach ( $categories as $cat ) {
						if(in_array($cat->term_id, $notifyscript_cats)) {
							$found = true;
						}
					}
					if(!$found) {
						$notifyscript_posts = explode(',', $this->get_option('notifyscript_pots'));
						if(!in_array($post->ID, $notifyscript_posts)) {
							return;
						}
					}
					
				}			
			}
	
			echo "\n" . $output;
		}
	}
	
    function admin_menu() {
        add_menu_page( 'NotifySnack', 'NotifySnack', 2, basename( __FILE__ ), array( &$this, 'admin_options_page' ), ( $this->url_path.'/images/icon.png' ) );
    }
    function admin_options_page() {
        if( !current_user_can( 'manage_options' ) ) {
            wp_die( 'You do not have sufficient permissions to access this page' );
        }
        
        if( isset( $_POST ) && !empty( $_POST ) ) {
            if( wp_verify_nonce( $_REQUEST[$this->namespace . '_update_wpnonce'], $this->namespace . '_options' ) ) {
                $data = array();

                foreach( $_POST as $key => $val ) {
                    $data[$key] = $this->sanitize_notifyscript( $val );
                }
				
				if(!isset($data['notifyscript_allpages'])) {
					$data['notifyscript_allpages'] = 'false';
				}
				
				if(!isset($data['notifyscript_allcats'])) {
					$data['notifyscript_allcats'] = 'false';
				}
				
				if(!isset($data['notifyscript_pages'])) {
					$data['notifyscript_pages'] = "";
				} else {
					$data['notifyscript_pages'] = implode(',', $data['notifyscript_pages']);
				}
				
				if(!isset($data['notifyscript_cats'])) {
					$data['notifyscript_cats'] = "";
				} else {
					$data['notifyscript_cats'] = implode(',', $data['notifyscript_cats']);
				}
				
				if(!isset($data['notifyscript_posts'])) {
					$data['notifyscript_posts'] = "";
				} else {
					$data['notifyscript_posts'] = implode(',', $data['notifyscript_posts']);
				}
				
				
                switch( $data['form_action'] ) {
                    case "update_options":
                        $options = array(
                            'notifyscript' => (string) $data['notifyscript'],
							'notifyscript_location' => (string) $data['notifyscript_location'],
							'notifyscript_showon' => (string) $data['notifyscript_showon'],
        					'notifyscript_allpages' => (string) $data['notifyscript_allpages'],
							'notifyscript_pages' => (string) $data['notifyscript_pages'],
							'notifyscript_allcats' => (string) $data['notifyscript_allcats'],
							'notifyscript_cats' => (string) $data['notifyscript_cats'],
							'notifyscript_posts' => (string) $data['notifyscript_posts']
                        );

                        update_option( $this->option_name, $options );
                        $this->options = get_option( $this->option_name );
                    break;
                }
            }
        }
        
        $page_title  = 'NotifySnack Plugin Settings';
        $namespace   = $this->namespace;
        $options     = $this->options;
        $defaults    = $this->defaults;
        $plugin_path = $this->url_path;
        
        foreach( $this->defaults as $name => $default_value ) {
            $$name = $this->get_option( $name );
        }
		
		$notifyscript_pages = explode(',', $notifyscript_pages);
		$notifyscript_cats = explode(',', $notifyscript_cats);
		$notifyscript_posts = explode(',', $notifyscript_posts);
		
		$args = array(
			'sort_order' => 'ASC',
			'sort_column' => 'post_title',
			'hierarchical' => 1,
			'exclude' => '',
			'include' => '',
			'meta_key' => '',
			'meta_value' => '',
			'authors' => '',
			'child_of' => 0,
			'parent' => -1,
			'exclude_tree' => '',
			'number' => '',
			'offset' => 0,
			'post_type' => 'page',
			'post_status' => 'publish'
		);
		$pages = get_pages($args);
		
		$args =	array( 
			 'type' => 'post',
			 'child_of' => 0,
			 'orderby' => 'name',
			 'order' => 'ASC',
			 'hide_empty' => true 
		);
		$categories = get_categories($args);
		
		include( dirname( __FILE__ ) . '/template.php' ); 

    }
        
    private function get_option( $option_name ) {
        if( !isset( $this->options ) || empty( $this->options ) ) {
            $this->options = get_option( $this->option_name, $this->defaults );
        }
        
        if( isset( $this->options[$option_name] ) ) {
            return $this->options[$option_name];    
        } elseif( isset( $this->defaults[$option_name] ) ) {
            return $this->defaults[$option_name];   
        }
        return false;
    }
        
    private function sanitize_notifyscript( $str="" ) {
        if ( !function_exists( 'wp_kses' ) ) {
             require_once( ABSPATH . 'wp-includes/kses.php' );
        }        global $allowedposttags;
        global $allowedprotocols;
        
        if ( is_string( $str ) ) {
            $str = htmlentities( stripslashes( $str ), ENT_QUOTES, 'UTF-8' );
	        $str = wp_kses( $str, $allowedposttags, $allowedprotocols );
        }
		
        return $str;
    }
	
	private function IsChecked($chkname, $value) {
        if(!empty($chkname)) {
            foreach($chkname as $chkval) {
                if((int)$chkval == $value) { return true; }
            }
        }
        return false;
    }
    
}

add_action( 'init', 'NotifySnackPlugin' );
function NotifySnackPlugin() {
    global $NotifySnackPlugin;
    $NotifySnackPlugin = new NotifySnackPlugin();
}

?>