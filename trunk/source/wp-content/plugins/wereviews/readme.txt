=== WordPress Review Engine ===
Contributors: Shaon
Donate link:  
Tags: review, product review, product rating, wordpress review, reviews, best review, review plugin, money making machine, affiliate review, affiliate revenue, affiliate earning, affiliates 
Requires at least: 2.0.2
Tested up to: 3.2.0
Stable tag: 4.3
 

WeReview - WordPress Review Engine, Just install and use, compatible with any theme. Convert your site to a money making machine just in few click. 
   

== Description ==

WeReview - WordPress Review Plugin will convert your site a money making machine just in few click. WeReview - WordPress Review Engine is compatible with any WordPress theme, So no additional modification will be required. Also WeReview - WordPress Review Engine comes with multiple review page style which will give your review page a different and exclusive look. WeReview - WordPress Review Plugin use custom post type feature which will enable your to use all seo facility like you are getting in post or page. 

= WordPress Review Engine Key Features: =
*	Compatible with all theme, so no additional modification required, you just can start using WeReview - WordPress Review Plugin with your existing theme.
*	WeReview - WordPress Review Plugin use custom post type feature
*	Customizable feature rating
*	Easy administration

= How to: = 
*	Use custom url http://domainname/reviews/ ( if permalink enabled, if not http://domainname/?post_type=reviews ) to see all reviews in blog archive format, update parmalink again if your see 404 error
*	Use short-code [wp_review] to list all reviews in tabular format

== Installation ==


1. Upload `wereviews` to the `/wp-content/plugins/`  directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Screenshots ==
1. Front-end review details page
2. Use short-code [wp_review] in page or post content to embed this table
3. All reviews (admin)
4. Add new review (admin)


== Changelog ==


= 1.1.1 =
* adjusted minor issue with price
* timthumb updated

= 1.1.0 =
* upgraded review short-code
* adjusted ribon styles
* adjusted css issue in admin screen

= 1.0.2 =
* review comments enabled

= 1.0.1 =
* fixed license issue, removed prettyphoto.js

= 1.0.0 =
* initial release

 